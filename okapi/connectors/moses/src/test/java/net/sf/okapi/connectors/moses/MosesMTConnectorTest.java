/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.moses;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.query.IQuery;
import net.sf.okapi.common.query.QueryResult;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class MosesMTConnectorTest {
	private static IQuery conn;

	@BeforeClass
	public static void setUp() throws Exception {
		conn = new MosesMTConnector();
		conn.setLanguages(LocaleId.FRENCH, LocaleId.ENGLISH);
		conn.open();
	}

	@Test
	public void dummyTest() {			
	}
	
	/*************************************************************
	 Start mosesserver with FR-EN sample model to run these tests
	 http://www.statmt.org/moses_steps.html
	 ************************************************************/
	
	//@Test
	public void plainTextQueryTest() {
		conn.query("Est une petite maison.");
		QueryResult qr = conn.next();
		Assert.assertEquals("Is a small house.", qr.target.toText());
		Assert.assertEquals(95, qr.getFuzzyScore());
		Assert.assertTrue(qr.fromMT());

		conn.query("Les forces armées du Pakistan -- cibles répétées d'attentats suicide -- sont démoralisées.");
		qr = conn.next();
		// FIXME: moses is going to require more cleanup than currently provided
		Assert.assertEquals(
				"Pakistan's armed forces -- repeated targets d'attentats suicide -- have become demoralized.",
				qr.target.toText());
	}

	//@Test
	public void textFragmentQueryTest() {
		TextFragment tf = new TextFragment("Est une ");
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("petite maison");
		tf.append(TagType.CLOSING, "b", "</b>");
		tf.append(".");
		conn.query(tf);
		QueryResult qr = conn.next();
		Assert.assertEquals("Is a small house.<b></b>", qr.target.toText());
	}
}
