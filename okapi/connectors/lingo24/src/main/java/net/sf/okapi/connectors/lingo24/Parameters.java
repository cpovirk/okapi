package net.sf.okapi.connectors.lingo24;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

/**
 * Connector for the <code>Lingo24Connector</code>.
 */
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String APIKEY = "userKey";
	
	public Parameters() {
	}
	
	public String getUserKey () {
		return getString(APIKEY);
	}

	public void setUserKey (String userKey) {
		setString(APIKEY, userKey);
	}

	@Override
	public void reset () {
		super.reset();
        setUserKey("");
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(APIKEY,
			"Lingo24 Premium MT API key",
			"The Lingo24 Premium MT API key to identify the application/user");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Lingo24 Premium MT Connector Settings", true, false);
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(APIKEY));
		tip.setPassword(true);
		return desc;
	}

}
