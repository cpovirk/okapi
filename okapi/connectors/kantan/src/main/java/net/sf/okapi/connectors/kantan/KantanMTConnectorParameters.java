package net.sf.okapi.connectors.kantan;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

public class KantanMTConnectorParameters extends StringParameters implements IEditorDescriptionProvider {
    private static final String PROFILE_NAME = "profileName";
    private static final String API_TOKEN = "apiToken";

    public KantanMTConnectorParameters() {
        super();
    }

    public void setProfileName(String profileName) {
        setString(PROFILE_NAME, profileName);
    }

    public String getProfileName() {
        return getString(PROFILE_NAME);
    }

    public void setApiToken(String apiToken) {
        setString(API_TOKEN, apiToken);
    }

    public String getApiToken() {
        return getString(API_TOKEN);
    }

    @Override
    public void reset() {
        super.reset();
        setProfileName("");
        setApiToken("");
    }

    @Override
    public ParametersDescription getParametersDescription() {
        ParametersDescription desc = new ParametersDescription(this);
        desc.add(PROFILE_NAME,
                "KantanMT Client Profile",
                "Name of the KantanMT Client Profile"
        );
        desc.add(API_TOKEN,
                "KantanMT Authorization Token",
                "KantanMT API Authorization Token"
        );

        return desc;
    }

    @Override
    public EditorDescription createEditorDescription(ParametersDescription parametersDescription) {
        EditorDescription desc = new EditorDescription("KantanMT Connector Settings", true, false);
        desc.addTextInputPart(parametersDescription.get(PROFILE_NAME));
        TextInputPart tipSecret = desc.addTextInputPart(parametersDescription.get(API_TOKEN));
        tipSecret.setPassword(true);

        return desc;
    }
}
