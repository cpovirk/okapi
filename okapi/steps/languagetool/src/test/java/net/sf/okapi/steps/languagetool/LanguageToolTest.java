/*
 * ===========================================================================
 * Copyright (C) 2013 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 * ===========================================================================
 */

package net.sf.okapi.steps.languagetool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Locale;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.ITSLQIAnnotations;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.languagetool.Language;

@RunWith(JUnit4.class)
public class LanguageToolTest {

	private LanguageTool lt;
	private final LocaleId srcLoc = new LocaleId("en-us");
	private final LocaleId trgLoc = new LocaleId("fr-fr");

	@Before
	public void setUp () {
		lt = new LanguageTool(null, srcLoc, trgLoc);
	}

	@Test
	public void simpleTest1 () {
		ITextUnit tu = new TextUnit("id", "original teext");
		tu.setTargetContent(trgLoc, new TextFragment("texte original"));
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		assertEquals("misspelling", anns.getFirstAnnotation(GenericAnnotationType.LQI).getString(GenericAnnotationType.LQI_TYPE));
		anns = tu.getTarget(trgLoc).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
		assertEquals("typographical", anns.getFirstAnnotation(GenericAnnotationType.LQI).getString(GenericAnnotationType.LQI_TYPE));
	}

	@Test
	public void testLTLanguages () {
		assertEquals("en-US", Language.getLanguageForLocale(new Locale("en", "US")).getShortNameWithVariant());
		assertEquals("en-US", Language.getLanguageForShortName("en-US").getShortNameWithVariant());
		assertEquals("en-US", Language.getLanguageForLocale(new Locale("en", "us")).getShortNameWithVariant());
		// Fails: assertEquals("en-US", Language.getLanguageForShortName("en-us").getShortNameWithVariant());
	}
	
	@Test
	public void simpleTest2 () {
		ITextUnit tu = new TextUnit("id", "File not found: %1");
		tu.setTargetContent(trgLoc, new TextFragment("Fichier non trouv\u00e9: %1"));
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNull(anns);
		anns = tu.getTarget(trgLoc).getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
		assertEquals(1, anns.getAllAnnotations().size());
	}

	@Test
	public void testWithCodes () {
		TextFragment tf = new TextFragment();
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("<#>");
		tf.append(TagType.CLOSING, "b", "</b>");
		ITextUnit tu = new TextUnit("id");
		tu.setSourceContent(tf);
		tu.setTargetContent(trgLoc, tf.clone());
		lt.run(tu);
		ITSLQIAnnotations anns = tu.getSource().getAnnotation(ITSLQIAnnotations.class);
		assertNotNull(anns);
//TODO: fix inline handling		
		GenericAnnotation ann = anns.getAnnotations(GenericAnnotationType.LQI).get(0);
		assertEquals(0, (int)ann.getInteger(GenericAnnotationType.LQI_XSTART));
		assertEquals(3, (int)ann.getInteger(GenericAnnotationType.LQI_XEND));
	}
}
