/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.annotation;

import java.util.Collection;
import java.util.HashMap;

import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.StartSubDocument;

/**
 * Annotation used to expose the %lt;phase-group&gt; element containing the multiple
 * phases in a %lt;file&gt; element when attached to a StartSubDocument event or
 * resolve the phase-name reference otherwise.
 */
public class XLIFFPhaseAnnotation implements IAnnotation {
	private HashMap<String, XLIFFPhase> phases = new HashMap<String, XLIFFPhase>();

	/**
	 * Add a dereferenced phase element to the annotation.
	 * @param phase - XLIFFPhase pulled from a StartSubDocument using the phase-name attribute.
	 */
	public void add(XLIFFPhase phase) {
		this.phases.put(phase.getPhaseName(), phase);
	}

	/**
	 * Add a parsed %lt;phase&gt; element to a StartSubDocument Event.
	 * @param phase the phase element
	 * @param startSubDoc the {@link StartSubDocument} Event
	 */
	public void add(XLIFFPhase phase, StartSubDocument startSubDoc) {
		this.phases.put(phase.getPhaseName(), phase);
		updatePhaseAnnotation(startSubDoc);
	}

	public XLIFFPhase get(String phaseName) {
		return this.phases.get(phaseName);
	}

	public XLIFFPhase getReferencedPhase() {
		Collection<XLIFFPhase> refPhases = this.phases.values();
		if (refPhases.size() == 1) {
			return refPhases.iterator().next();
		}
		return null;
	}

	public void updatePhaseAnnotation(StartSubDocument startSubDoc) {
		Property phasePlaceholder = (startSubDoc.getProperty(Property.XLIFF_PHASE) == null) ?
			new Property(Property.XLIFF_PHASE, "") : startSubDoc.getProperty(Property.XLIFF_PHASE);
		phasePlaceholder.setValue(toXML());
	}

	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<phase-group>");
		for (XLIFFPhase phase : phases.values()) {
			sb.append(phase.toXML());
		}
		sb.append("</phase-group>");
		return sb.toString();
	}
}
