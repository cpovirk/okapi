package net.sf.okapi.common.resource;

import java.util.Comparator;


public class CodeComparatorOnId implements Comparator<Code> {

	@Override
	public int compare(Code c1, Code c2) {
		if (c1.getId() == c2.getId() && c1.getTagType() == c2.getTagType()) {			
			return 0;
		} else if (c1.getId() == c2.getId() && c1.getTagType() != c2.getTagType()) {
			return c1.getTagType().compareTo(c2.getTagType()); 
		} else {
			return Integer.compare(c1.getId(), c2.getId());
		}
	}
}
