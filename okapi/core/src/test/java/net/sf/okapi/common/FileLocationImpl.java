package net.sf.okapi.common;

import java.net.MalformedURLException;
import java.net.URL;

class FileLocationImpl {
	static URL makeUrlFromName(Class<?> clazz, String name, boolean out) {

		URL result = null;
		String tmpName = null == name ? "" : name;

		URL t;
		if (tmpName.startsWith("/")) {
			t = clazz.getResource("/");
			tmpName = tmpName.substring(1);
		} else {
			t = clazz.getResource("");
		}

		try {
			String filePart = t.getFile() + tmpName;
			if (out)
				filePart = filePart.replace("/target/test-classes/", "/target/test-classes/out/");
			result = new URL(t.getProtocol(), t.getHost(), t.getPort(), filePart);
		} catch (MalformedURLException e) {
		}

		return result;
	}
}

