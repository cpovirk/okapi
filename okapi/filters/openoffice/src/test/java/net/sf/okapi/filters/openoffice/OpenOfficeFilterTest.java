/*===========================================================================
  Copyright (C) 2008-2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.openoffice;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.TestUtil;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.filters.RoundTripComparison;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.ITextUnit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DataProviderRunner.class)
public class OpenOfficeFilterTest {

	private OpenOfficeFilter filter;
	private String root;
	private LocaleId locEN = LocaleId.fromString("en");

	@Before
	public void setUp() {
		filter = new OpenOfficeFilter();
		root = TestUtil.getParentDir(this.getClass(), "/TestDocument01.odt_content.xml");
	}

	@Test
	public void testDefaultInfo () {
		assertNotNull(filter.getParameters());
		assertNotNull(filter.getName());
		List<FilterConfiguration> list = filter.getConfigurations();
		assertNotNull(list);
		assertTrue(list.size()>0);
	}

	@Test
	public void testFirstTextUnit () {
		ITextUnit tu = getTextUnit(1, "TestDocument01.odt");
		assertNotNull(tu);
		assertEquals("Heading 1", tu.getSource().toString());
	}

	private ITextUnit getTextUnit(int i, String fileName) {
		return FilterTestDriver.getTextUnit(filter,
				new InputDocument(root+fileName, null),
				"UTF-8", locEN, locEN, i);
	}

	@DataProvider
	public static Object[][] testMetadataExtractionProvider () {
		return new Object[][] {
				{
						new ParametersBuilder().extractMetadata(true).build(),
						new String[] {
								"Text on the first page.",
								"Text on the second page.",
								"Author: Test",
								//reminder: raw xml in a text unit
								"Page <text:page-number text:select-page=\"current\">2</text:page-number> of <text:page-count>2</text:page-count>",
								"Test document meta comments",
								"met keywod1",
								"keyword2",
								"Test document meta description",
								"Test document meta title",
								"Test custom property's value",
						},
				},
				{
						new ParametersBuilder().extractMetadata(false).build(),
						new String[] {
								"Text on the first page.",
								"Text on the second page.",
								"Author: Test",
								//reminder: raw xml in a text unit
								"Page <text:page-number text:select-page=\"current\">2</text:page-number> of <text:page-count>2</text:page-count>",
						},
				},
		};
	}

	@Test
	@UseDataProvider("testMetadataExtractionProvider")
	public void testMetadataExtraction (Parameters params, String[] expectedTexts) {
		List<ITextUnit> textUnits = FilterTestDriver.filterTextUnits(
				getEvents(root + "TestDocumentWithMetadata.odt", params));
		assertThat(textUnits.size(), is(expectedTexts.length));
		for (int i = 0; i < textUnits.size(); i++) {
			Assert.assertThat(textUnits.get(i).getSource().toString(), equalTo(expectedTexts[i]));
		}
	}

	private ArrayList<Event> getEvents (String path, Parameters params) {
		return FilterTestDriver.getEvents(filter, new RawDocument(new File(path).toURI(), "UTF-8", locEN), params);
	}

	@Test
	public void testStartDocument () {
		assertTrue("Problem in StartDocument", FilterTestDriver.testStartDocument(filter,
			new InputDocument(root+"TestDocument01.odt", null),
			"UTF-8", locEN, locEN));
	}
	
	@Test
	public void testDoubleExtraction () throws URISyntaxException {
		ArrayList<InputDocument> list = new ArrayList<InputDocument>();
		list.add(new InputDocument(root+"TestSpreadsheet01.ods", null));
		list.add(new InputDocument(root+"TestDocument01.odt", null));
		list.add(new InputDocument(root+"TestDocument02.odt", null));
		list.add(new InputDocument(root+"TestDocument03.odt", null));
		list.add(new InputDocument(root+"TestDocument04.odt", null));
		list.add(new InputDocument(root+"TestDocument05.odt", null));
		list.add(new InputDocument(root+"TestDocument06.odt", null));
		list.add(new InputDocument(root+"TestDrawing01.odg", null));
		list.add(new InputDocument(root+"TestPresentation01.odp", null));
		list.add(new InputDocument(root+"TestDocument_WithITS.odt", null));
		list.add(new InputDocument(root+"TestDocumentWithMetadata.odt", null));
		RoundTripComparison rtc = new RoundTripComparison();
		assertTrue(rtc.executeCompare(filter, list, "UTF-8", locEN, locEN, "out"));
	}

}
