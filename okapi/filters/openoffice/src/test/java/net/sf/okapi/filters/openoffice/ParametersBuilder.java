package net.sf.okapi.filters.openoffice;


class ParametersBuilder {
    private Parameters params = new Parameters();

    Parameters build(){
        return params;
    }

    ParametersBuilder extractMetadata(boolean param){
        this.params.setExtractMetadata(param);
        return this;
    }
}
