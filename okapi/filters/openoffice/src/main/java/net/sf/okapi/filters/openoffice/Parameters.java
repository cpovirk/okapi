/*===========================================================================
  Copyright (C) 2008-2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.openoffice;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {
	
	private static final String EXTRACTNOTES = "extractNotes";
	private static final String EXTRACTREFERENCES = "extractReferences";
	private static final String CONVERTSPACESTABS = "convertSpacesTabs";
	private static final String EXTRACTMETADATA = "extractMetadata";
	
	public Parameters () {
		super();
	}
	
	public void reset () {
		super.reset();
		setExtractNotes(false);
		setExtractReferences(false);
		setConvertSpacesTabs(false);
		setSimplifierRules(null);
		setExtractMetadata(true);
	}

	public boolean getExtractNotes() {
		return getBoolean(EXTRACTNOTES);
	}

	public void setExtractNotes(boolean extractNotes) {
		setBoolean(EXTRACTNOTES, extractNotes);
	}

	public boolean getExtractReferences() {
		return getBoolean(EXTRACTREFERENCES);
	}

	public void setExtractReferences(boolean extractReferences) {
		setBoolean(EXTRACTREFERENCES, extractReferences);
	}

	public boolean getConvertSpacesTabs() {
		return getBoolean(CONVERTSPACESTABS);
	}

	public void setConvertSpacesTabs(boolean convertSpacesTabs) {
		setBoolean(CONVERTSPACESTABS, convertSpacesTabs);
	}
	
	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
    }

	public boolean getExtractMetadata() {
		return getBoolean(EXTRACTMETADATA);
	}

	public void setExtractMetadata(boolean extractMetadata) {
		setBoolean(EXTRACTMETADATA, extractMetadata);
	}
}
