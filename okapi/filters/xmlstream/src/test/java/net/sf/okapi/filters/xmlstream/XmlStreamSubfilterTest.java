/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/


package net.sf.okapi.filters.xmlstream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.StartGroup;
import net.sf.okapi.common.resource.TextUnit;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.filters.xmlstream.integration.XmlStreamTestUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class XmlStreamSubfilterTest {
	
	private static LocaleId locEN = LocaleId.fromString("en");
	private XmlStreamFilter filter;
	
	@Before
	public void setUp() throws Exception {
		filter = new XmlStreamFilter();
	}
	
	@Test
	public void testSimple() throws Exception {
		URL configUrl = XmlStreamConfigurationTest.class
				.getResource("/subfilter-simple.yml");
		URL inputUrl = XmlStreamConfigurationTest.class
				.getResource("/subfilter-simple.xml");
		RawDocument rd = new RawDocument(inputUrl.toURI(), "UTF-8", locEN);
		List<Event> events = getEvents(filter, rd, configUrl);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Translate me.", tu.getSource().toString());
		
		// Make sure only one TU was produced
		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNull(tu);
	}
	
	@Test
	public void testNestedTextunits() throws Exception {
		URL configUrl = XmlStreamConfigurationTest.class
				.getResource("/subfilter-simple.yml");
		List<Event> events;
		String xml = "<xml><x1>foo</x1>bar</xml>";
		RawDocument rd = new RawDocument(xml, locEN);
		events = getEvents(filter, rd, configUrl);
		assertEquals("foo", FilterTestDriver.getTextUnit(events, 1).getSource().toString());
		assertEquals("bar", FilterTestDriver.getTextUnit(events, 2).getSource().toString());
	}
	
	@Test
	public void testTranslateAttributeSubfilter() throws Exception {
		URL configUrl = XmlStreamConfigurationTest.class
				.getResource("/translate-attr-subfilter.yml");
		URL inputUrl = XmlStreamConfigurationTest.class
				.getResource("/translate-attr-subfilter.xml");
		RawDocument rd = new RawDocument(inputUrl.toURI(), "UTF-8", locEN);
		List<Event> events = getEvents(filter, rd, configUrl);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("Translate me.", tu.getSource().toString());
		
		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("<a>Translate me 4.<a>", tu.getSource().toString());
		
		tu = FilterTestDriver.getTextUnit(events, 3);
		assertNull(tu);
	}
	
	@Test
	public void testCdataSubfilter() throws Exception {
		URL configUrl = getClass().getResource("/cdataAsHTML.yml");
		URL inputUrl = getClass().getResource("/simple_cdata.xml");
		RawDocument rd = new RawDocument(inputUrl.toURI(), "UTF-8", locEN);
		List<Event> events = getEvents(filter, rd, configUrl);
		assertEquals("About", FilterTestDriver.getTextUnit(events, 1).getSource().toString());
		assertEquals("Testing", FilterTestDriver.getTextUnit(events, 2).getSource().toString());
		assertEquals("<b>Test</b> with some <u>HTML</u> <i>tags</i>.", 
					 FilterTestDriver.getTextUnit(events, 3).getSource().toString());
		// Make sure there's no "bonus" segment containing the placeholder that
		// references the CDATA
		assertNull(FilterTestDriver.getTextUnit(events, 4));
	}
	
	@Test
	public void testCdataSubfilterEmptyElement() throws Exception {
		URL configUrl = getClass().getResource("/cdataAsHTML.yml");
		URL inputUrl = getClass().getResource("/empty_element.xml");
		RawDocument rd = new RawDocument(inputUrl.toURI(), "UTF-8", locEN);
		List<Event> events = getEvents(filter, rd, configUrl);	
		assertEquals("foobar", 
					 FilterTestDriver.getTextUnit(events, 1).getSource().toString());
		// Make sure there's no "bonus" segment containing the placeholder that
		// references the CDATA
		assertNull(FilterTestDriver.getTextUnit(events, 2));
	}

	// Test for Issue #339: interaction between subfiltering and
	// GROUP rules.
	@Test
	public void testCdataMerging() throws Exception {
		URL configUrl = getClass().getResource("/cdataWithGroup.yml");
		URL inputUrl = getClass().getResource("/cdataWithGroup.xml");
		RawDocument rd = new RawDocument(inputUrl.toURI(), "UTF-8", locEN);
		List<Event> events = getEvents(filter, rd, configUrl);
		assertEquals("Test", FilterTestDriver.getTextUnit(events, 1).getSource().toString());

		// Test to make sure the skeleton all ends up in the right place
		ArrayList<Event> expectedEvents = new ArrayList<Event>();
		addStartEvents(expectedEvents);
		addDocumentPart(expectedEvents, "dp1", "<Solution>");
		addStartGroup(expectedEvents, "sg1", "<RESOLUTION>");
		expectedEvents.add(new Event(EventType.START_SUBFILTER));
		addTextUnit(expectedEvents, "sg1_tu1", "sd1_1", "Test", "<p>[#$$self$]");
		addDocumentPart(expectedEvents, "sg1_dp1", "</p>");
		expectedEvents.add(new Event(EventType.END_SUBFILTER));
		addEndGroup(expectedEvents, "sg1", "<![CDATA[[#$sg1_ssf1]]]></RESOLUTION>");
		addDocumentPart(expectedEvents, "dp3", "</Solution>");
		addDocumentPart(expectedEvents, "dp4", "\n");
		addEndEvents(expectedEvents);
		Iterator<Event> expectedIt = expectedEvents.iterator();
		for (Event e : events) {
			Event ee = expectedIt.next();
			if (!FilterTestDriver.laxCompareEvent(ee, e)) {
				fail("Event mismatch: expected " + ee + " but found " + e);
			}
		}
	}
	
	@Test
	public void issue375() throws Exception {
		// doesn't group back nested translation
		URL configUrl = XmlStreamConfigurationTest.class
				.getResource("/Issue375.yml");
		String snippet = 
			"<SOLUTIONS>\n" +
			"<![CDATA[Attachments in <img width=\"13\" height=\"15\" src=\"mail-p_attach_all_01.png\" alt=\"paper clip\" /> end tag]]>\n" +
			"</SOLUTIONS>";
		RawDocument rd = new RawDocument(snippet, locEN);
		List<Event> events = getEvents(filter, rd, configUrl);
		
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("paper clip", tu.getSource().toString());
		
		tu = FilterTestDriver.getTextUnit(events, 2);
		assertNotNull(tu);
		assertEquals("Attachments in [#$sd1_dp1] end tag", tu.getSource().toString());
		
		tu = FilterTestDriver.getTextUnit(events, 3);
		assertNull(tu);
		
		assertEquals(snippet, XmlStreamTestUtils.generateOutput(
				XmlStreamTestUtils.getEvents(snippet, filter, configUrl), snippet, locEN,
				filter));
	}

	private void addStartEvents(ArrayList<Event> events) {		
		events.add(new Event(EventType.START_DOCUMENT, new StartDocument("sd1")));
	}

	private void addEndEvents(ArrayList<Event> events) {
		events.add(new Event(EventType.END_DOCUMENT, new Ending("ed2")));
	}
	
	private void addDocumentPart(ArrayList<Event> events, String id, String skeleton) {
		events.add(new Event(EventType.DOCUMENT_PART, 
				new DocumentPart(id, false, new GenericSkeleton(skeleton))));
	}

	private void addStartGroup(ArrayList<Event> events, String id, String skeleton) {
		StartGroup sg = new StartGroup(null, id);
		sg.setSkeleton(new GenericSkeleton(skeleton));
		events.add(new Event(EventType.START_GROUP, sg)); 
	}
	
	private void addEndGroup(ArrayList<Event> events, String id, String skeleton) {
		Ending eg = new Ending(id);
		eg.setSkeleton(new GenericSkeleton(skeleton));
		events.add(new Event(EventType.END_GROUP, eg));
	}
	
	private void addTextUnit(ArrayList<Event> events, String id, String name, String text, String skeleton) {
		TextUnit tu = new TextUnit(id, text, false);
		tu.setName(name);
		tu.setType("paragraph");
		tu.setSkeleton(new GenericSkeleton(skeleton));
		events.add(new Event(EventType.TEXT_UNIT, tu));
	}
	
	private ArrayList<Event> getEvents(XmlStreamFilter filter, RawDocument doc, URL params) {    
        FilterConfigurationMapper mapper = new FilterConfigurationMapper();
        mapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
        filter.setFilterConfigurationMapper(mapper);
        return FilterTestDriver.getEvents(filter, doc, new Parameters(params));
    }   
}
