package net.sf.okapi.filters.openxml;


class ConditionalParametersBuilder {
    private ConditionalParameters params = new ConditionalParameters();

    ConditionalParameters build(){
        return params;
    }

    ConditionalParametersBuilder replaceNoBreakHyphenTag(boolean param){
        this.params.setReplaceNoBreakHyphenTag(param);
        return this;
    }

    ConditionalParametersBuilder ignoreSoftHyphenTag(boolean param){
        this.params.setIgnoreSoftHyphenTag(param);
        return this;
    }

    ConditionalParametersBuilder addLineSeparatorCharacter(boolean param){
        this.params.setAddLineSeparatorCharacter(param);
        return this;
    }

    ConditionalParametersBuilder cleanupAggressively(boolean param){
        this.params.setCleanupAggressively(param);
        return this;
    }

    ConditionalParametersBuilder addTabAsCharacter(boolean param){
        this.params.setAddTabAsCharacter(param);
        return this;
    }
}
