package net.sf.okapi.filters.openxml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.RawDocument;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Miscellaneous OOXML tests.
 */
@RunWith(JUnit4.class)
public class OpenXMLTest {
	private LocaleId locENUS = LocaleId.fromString("en-us");
	
	/**
	 * Test to ensure the filter can handle an OOXML package in
	 * which the [Content Types].xml document does not appear 
	 * as the first entry in the ZIP archive.
	 * @throws Exception 
	 */
	@Test
	public void testReorderedZipPackage() throws Exception {
		OpenXMLFilter filter = new OpenXMLFilter();
		URL url = getClass().getResource("/reordered-zip.docx");
		RawDocument doc = new RawDocument(url.toURI(),"UTF-8", locENUS);
		ArrayList<Event> events = getEvents(filter, doc);
		ITextUnit tu = FilterTestDriver.getTextUnit(events, 1);
		assertNotNull(tu);
		assertEquals("This is a test.", tu.getSource().getCodedText());
		tu = FilterTestDriver.getTextUnit(events, 2);
		assertEquals("Untitled document.docx", tu.getSource().toString());
	}

	/**
	 * Test to ensure that the filter parses the file metadata 
	 * in order to present PPTX slides for translation in the order
	 * they are viewed by the user.
	 * @throws Exception
	 */
	@Test
	public void testSlideReordering() throws Exception {
		OpenXMLFilter filter = new OpenXMLFilter();
		URL url = getClass().getResource("/Okapi-325.pptx");
		RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
		ArrayList<Event> events = getEvents(filter, doc);
		checkTu(events, 1, "<run1>Sample Presentation</run1>");
		checkTu(events, 2, "<run1>This is slide 1</run1>");
		checkTu(events, 3, ""); // Empty TU for some reason
		checkTu(events, 4, "<run1>This is slide 2</run1>");
		checkTu(events, 5, "<run1>This is slide 3</run1>");
	}

	/**
	 * Test that we expose document properties for PowerPoint files.
	 */
	@Test
	public void testPPTXDocProperties() throws Exception {
		OpenXMLFilter filter = new OpenXMLFilter();
		ConditionalParameters params = (ConditionalParameters)filter.getParameters();
		params.setTranslateDocProperties(true);
		params.setTranslateComments(false);
		params.setTranslatePowerpointMasters(false);
		params.setTranslatePowerpointNotes(false);
		URL url = getClass().getResource("/DocProperties.pptx");
		RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
		ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
		assertEquals(10, events.size());
		// The first 4 are body that we don't care about
		checkTu(events, 5, "Test of OOXML filter");
		checkTu(events, 6, "Okapi OOXML Filter");
		checkTu(events, 7, "Chase Tingley");
		checkTu(events, 8, "Okapi, filtering, OOXML, PPTX");
		checkTu(events, 9, "This is document property comment.");
		checkTu(events, 10, "Filters");
	}

	/**
	 * Verify that disabling the option also works.
	 */
	@Test
	public void testPPTXIgnoreDocProperties() throws Exception {
		OpenXMLFilter filter = new OpenXMLFilter();
		ConditionalParameters params = (ConditionalParameters)filter.getParameters();
		params.setTranslateDocProperties(false);
		params.setTranslateComments(false);
		params.setTranslatePowerpointMasters(false);
		params.setTranslatePowerpointNotes(false);
		URL url = getClass().getResource("/DocProperties.pptx");
		RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
		ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
		// Only the 4 body segments are still there
		assertEquals(4, events.size());
	}

    /**
     * Test that PPTX comments are extracted.
     */
    @Test
    public void testPPTXComments() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateComments(true);
        params.setTranslateDocProperties(false);
        params.setTranslatePowerpointMasters(false);
        params.setTranslatePowerpointNotes(false);
        URL url = getClass().getResource("/Comments.pptx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(2, events.size());
        assertEquals("Comment on the title slide", events.get(0).getTextUnit().getSource().getCodedText());
        assertEquals("This is a comment on a slide body.", events.get(1).getTextUnit().getSource().getCodedText());
    }

    /**
     * Verify that disabling the option also works.
     */
    @Test
    public void testPPTXIgnoreComments() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateComments(false);
        params.setTranslateDocProperties(false);
        params.setTranslatePowerpointMasters(false);
        params.setTranslatePowerpointNotes(false);
        URL url = getClass().getResource("/Comments.pptx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(0, events.size());
    }

    @Test
    public void testWordDontTranslateHiddenText() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateWordHidden(false);
        params.setTranslateDocProperties(false);
        URL url = getClass().getResource("/Hidden.docx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        // Legacy behavior here is to emit 6 segments:
        // - One with content
        // - 3 with codes (hidden inline text)
        // - 2 completely empty (not sure why)
        assertEquals(1, events.size());
        // Simply checking the value of getSource().toString() isn't enough, because for
        // the 3rd and 4th TUs, it's the same regardless of whether or not the hidden text
        // is translated.
        assertEquals("Here is the message of Jeremiah.", getCodedText(events.get(0)));
    }

	private void dump(List<Event> events) {
		for (Event e : events) {
			if (!(e.getResource() instanceof ITextUnit)) continue;
			System.out.println(e.getTextUnit().getSource().getCodedText());
			System.out.println(e.getTextUnit().getSource().toString());
		}
	}

    @Test
    public void testWordTranslateHiddenText() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateWordHidden(true);
        params.setTranslateDocProperties(false);
        URL url = getClass().getResource("/Hidden.docx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(4, events.size());
        assertEquals("Here is the hidden message written by the hand of Jeremiah.",
        			 getCodedText(events.get(0)));
        assertEquals("Here is the message of Isaiah.", getCodedText(events.get(1)));
        assertEquals("Here is the message of Daniel.", getCodedText(events.get(2)));
        assertEquals("Here is the message of Peter, James & John.", getCodedText(events.get(3)));
    }

    @Test
    public void testXLSXOnlyExtractStringsNotNumbers() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateDocProperties(false);
        URL url = getClass().getResource("/sample.xlsx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(10, events.size());
        checkTu(events, 1, "Lorem");
        checkTu(events, 2, "ipsum");
        checkTu(events, 3, "dolor");
        checkTu(events, 4, "sit");
        checkTu(events, 5, "amet");
        checkTu(events, 6, "consectetuer");
        checkTu(events, 7, "adipiscing");
        checkTu(events, 8, "elit");
        checkTu(events, 9, "Nunc");
        checkTu(events, 10, "at");
    }

    /**
     * This test now captures the intended ordering behavior of the
     * string table, which is to expose strings in the order they appear
     * to the user, not the order in which they appear in the original
     * string table.
     */
    @Test
    public void testXLSXOrdering() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateDocProperties(false);
        URL url = getClass().getResource("/ordering.xlsx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(7, events.size());
        checkTu(events, 1, "Cell A2");
        checkTu(events, 2, "Cell B2");
        checkTu(events, 3, "Cell C3");
        checkTu(events, 4, "Sheet 2, Cell A1");
        checkTu(events, 5, "Sheet2, Cell B2");
        checkTu(events, 6, "Sheet2, Cell A3");
        checkTu(events, 7, "Sheet 3, Cell A1");
    }

    /**
     * Test for Excel column excludes.
     */
    @Test
    public void testXLSXExcludeAllColumns() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateDocProperties(false);
        params.setTranslateExcelExcludeColumns(true);
        params.tsExcelExcludedColumns = new TreeSet<String>();
        params.tsExcelExcludedColumns.add("1A");
        params.tsExcelExcludedColumns.add("1B");
        URL url = getClass().getResource("/columns.xlsx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        // Current behavior seems to be exposing them as placeholders
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(0, events.size());
        // Make sure it also works on styled text
        RawDocument rd2 = new RawDocument(getClass().getResource("/cell_styling.xlsx").toURI(), "UTF-8", locENUS);
        events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(0, events.size());
        rd2.close();
    }

    /**
     * Test the case where the same string occurs in both excluded and non-excluded
     * contexts.
     */
    @Test
    public void testPartialExclusionFromColumns() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateDocProperties(false);

        // Parse once with default params, we should get both cells
        URL url = getClass().getResource("/shared_string_in_two_columns.xlsx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(2, events.size());
        assertEquals("Danger", events.get(0).getTextUnit().getSource().toString());
        assertEquals("Danger", events.get(1).getTextUnit().getSource().toString());

        // Now with excludes set, we only get one
        params.setTranslateExcelExcludeColumns(true);
        params.tsExcelExcludedColumns = new TreeSet<String>();
        params.tsExcelExcludedColumns.add("1A");
        doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(1, events.size());
        assertEquals("Danger", events.get(0).getTextUnit().getSource().toString());
    }

    @Test
    public void testSmartQuotes() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateDocProperties(false);
        URL url = getClass().getResource("/smartquotes.docx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(1, events.size());
        assertEquals("“Smart quotes”", events.get(0).getTextUnit().getSource().toString());
    }

    @Test
    public void testTabAsCharacter() throws Exception {
    	OpenXMLFilter filter = new OpenXMLFilter();
    	ConditionalParameters params = (ConditionalParameters)filter.getParameters();
    	params.setAddTabAsCharacter(true);
    	params.setTranslateDocProperties(false);
    	URL url = getClass().getResource("/Document-with-tabs.docx");
    	RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(1, events.size());
        assertEquals("Before\tafter.", events.get(0).getTextUnit().getSource().getCodedText());
    }

    @Test
    public void testTabAsCharacter2() throws Exception {
    	OpenXMLFilter filter = new OpenXMLFilter();
    	ConditionalParameters params = (ConditionalParameters)filter.getParameters();
    	params.setAddTabAsCharacter(true);
    	params.setTranslateDocProperties(false);
    	URL url = getClass().getResource("/Document-with-tabs-2.docx");
    	RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(1, events.size());
        assertEquals("Before\tafter.", events.get(0).getTextUnit().getSource().getCodedText());
    }

    @Test
    public void testTabAsTag() throws Exception {
    	OpenXMLFilter filter = new OpenXMLFilter();
    	ConditionalParameters params = (ConditionalParameters)filter.getParameters();
    	params.setAddTabAsCharacter(false);
    	params.setTranslateDocProperties(false);
    	URL url = getClass().getResource("/Document-with-tabs.docx");
    	RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(1, events.size());
        assertEquals("Beforeafter.", events.get(0).getTextUnit().getSource().getCodedText());
    }

    @Test
    public void testLineBreakAsCharacter() throws Exception {
    	OpenXMLFilter filter = new OpenXMLFilter();
    	ConditionalParameters params = (ConditionalParameters)filter.getParameters();
    	params.setAddLineSeparatorCharacter(true);
    	params.setTranslateDocProperties(false);
    	URL url = getClass().getResource("/Document-with-soft-linebreaks.docx");
    	RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(1, events.size());
        assertEquals("First line\nsecond line.", events.get(0).getTextUnit().getSource().getCodedText());
    }

    @Test
    public void testLineBreakAsTag() throws Exception {
    	OpenXMLFilter filter = new OpenXMLFilter();
    	ConditionalParameters params = (ConditionalParameters)filter.getParameters();
    	params.setAddLineSeparatorCharacter(false);
    	params.setTranslateDocProperties(false);
    	URL url = getClass().getResource("/Document-with-soft-linebreaks.docx");
    	RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(1, events.size());
        assertEquals("First linesecond line.", events.get(0).getTextUnit().getSource().getCodedText());
    }

    @Test
    public void testExcludeAllColors() throws Exception {
        OpenXMLFilter filter = new OpenXMLFilter();
        ConditionalParameters params = (ConditionalParameters)filter.getParameters();
        params.setTranslateDocProperties(false);
        params.setTranslateExcelExcludeColors(true);
        params.tsExcelExcludedColors = new TreeSet<String>();
        params.tsExcelExcludedColors.add("FF800000"); // dark red
        params.tsExcelExcludedColors.add("FFFF0000"); // red
        params.tsExcelExcludedColors.add("FFFF6600"); // orange
        params.tsExcelExcludedColors.add("FFFFFF00"); // yellow
        params.tsExcelExcludedColors.add("FFCCFFCC"); // light green
        params.tsExcelExcludedColors.add("FF008000"); // green
        params.tsExcelExcludedColors.add("FF3366FF"); // light blue
        params.tsExcelExcludedColors.add("FF0000FF"); // blue
        params.tsExcelExcludedColors.add("FF000090"); // dark blue
        params.tsExcelExcludedColors.add("FF660066"); // purple
        URL url = getClass().getResource("/standardcolors.xlsx");
        RawDocument doc = new RawDocument(url.toURI(), "UTF-8", locENUS);
        ArrayList<Event> events = FilterTestDriver.getTextUnitEvents(filter, doc);
        assertEquals(0, events.size());
    }

    private String getCodedText(Event e) {
        return e.getTextUnit().getSource().getCodedText();
    }

    private void checkTu(ArrayList<Event> events, int i, String gold) {
        ITextUnit tu = FilterTestDriver.getTextUnit(events, i);
        assertNotNull(tu);
        assertEquals(gold, tu.getSource().toString());
    }

	private ArrayList<Event> getEvents(OpenXMLFilter filter, RawDocument doc) {
        ArrayList<Event> list = new ArrayList<Event>();
        filter.open(doc, false);
        while (filter.hasNext()) {
            Event event = filter.next();
            list.add(event);
        }
        filter.close();
        return list;
    }
}
