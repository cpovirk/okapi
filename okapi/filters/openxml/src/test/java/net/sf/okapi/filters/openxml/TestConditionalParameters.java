package net.sf.okapi.filters.openxml;

import java.util.Collections;
import java.util.HashSet;

import org.junit.*;

import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestConditionalParameters {

	@Test
	public void testFindExcludedColumnsForSheetNumber() {
        String yaml =
				"#v1\n" +
				"bPreferenceTranslateDocProperties.b=false\n" +
				"bPreferenceTranslateComments.b=false\n" +
				"bPreferenceTranslatePowerpointNotes.b=true\n" +
				"bPreferenceTranslatePowerpointMasters.b=true\n" +
				"bPreferenceTranslateWordHeadersFooters.b=true\n" +
				"bPreferenceTranslateWordAllStyles.b=true\n" +
				"bPreferenceTranslateWordHidden.b=false\n" +
				"bPreferenceTranslateExcelExcludeColors.b=false\n" +
				"bPreferenceTranslateExcelExcludeColumns.b=true\n" +
				"tsExcelExcludedColors.i=0\n" +
				"tsExcelExcludedColumns.i=4\n" +
				"tsExcludeWordStyles.i=0\n" +
				"zzz0=1A\n" +
				"zzz1=1B\n" +
				"zzz2=3A\n" +
				"zzz3=3D\n";
        ConditionalParameters params = new ConditionalParameters();
        params.fromString(yaml);
        HashSet<String> expected1 = new HashSet<String>();
        expected1.add("A");
        expected1.add("B");
        HashSet<String> expected3 = new HashSet<String>();
        expected3.add("A");
        expected3.add("D");
        assertEquals(expected1, params.findExcludedColumnsForSheetNumber(1));
        assertEquals(Collections.emptySet(), params.findExcludedColumnsForSheetNumber(2));
        assertEquals(expected3, params.findExcludedColumnsForSheetNumber(3));
        // Sheets past 3 use the configuration for "sheet 3"
        assertEquals(expected3, params.findExcludedColumnsForSheetNumber(4));
	}

	@Test
	public void testDontExcludeColumnsIfOptionDisabled() {
        String yaml =
				"#v1\n" +
				"bPreferenceTranslateDocProperties.b=false\n" +
				"bPreferenceTranslateComments.b=false\n" +
				"bPreferenceTranslatePowerpointNotes.b=true\n" +
				"bPreferenceTranslatePowerpointMasters.b=true\n" +
				"bPreferenceTranslateWordHeadersFooters.b=true\n" +
				"bPreferenceTranslateWordAllStyles.b=true\n" +
				"bPreferenceTranslateWordHidden.b=false\n" +
				"bPreferenceTranslateExcelExcludeColors.b=false\n" +
				"bPreferenceTranslateExcelExcludeColumns.b=false\n" + // <--- don't actually exclude anything
				"tsExcelExcludedColors.i=0\n" +
				"tsExcelExcludedColumns.i=4\n" +
				"tsExcludeWordStyles.i=0\n" +
				"zzz0=1A\n" +
				"zzz1=1B\n" +
				"zzz2=3A\n" +
				"zzz3=3D\n";
        ConditionalParameters params = new ConditionalParameters();
        params.fromString(yaml);
        assertEquals(Collections.emptySet(), params.findExcludedColumnsForSheetNumber(1));
        assertEquals(Collections.emptySet(), params.findExcludedColumnsForSheetNumber(2));
        assertEquals(Collections.emptySet(), params.findExcludedColumnsForSheetNumber(3));
        assertEquals(Collections.emptySet(), params.findExcludedColumnsForSheetNumber(4));
	}
}
