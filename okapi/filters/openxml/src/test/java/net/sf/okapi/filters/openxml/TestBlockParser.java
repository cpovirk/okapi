package net.sf.okapi.filters.openxml;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.ITextUnit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.isParagraphStartEvent;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class TestBlockParser {
	private XMLFactories factories = new XMLFactoriesForTest();
	private ConditionalParameters defaultParams = new ConditionalParameters();

	@Test
	public void testTab() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-tab.xml"), defaultParams);
		assertEquals(3, tus.size());
		assertEquals("<tags1/>Text after tab.", tus.get(0).getSource().toString());
		assertEquals("Text before tab.<tags1/>", tus.get(1).getSource().toString());
		assertEquals("Text before<tags1/>and after tab.", tus.get(2).getSource().toString());
	}

	@Test
	public void testTabAsChar() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setAddTabAsCharacter(true);
		List<ITextUnit> tus = parseTextUnits(getReader("document-tab.xml"), params);
		assertEquals(4, tus.size());
		assertEquals("\t", tus.get(0).getSource().toString());
		assertEquals("\tText after tab.", tus.get(1).getSource().toString());
		assertEquals("Text before tab.\t", tus.get(2).getSource().toString());
		assertEquals("Text before\tand after tab.", tus.get(3).getSource().toString());
	}

	@Test
	public void testFieldAndTab() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-field-and-tab.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals("Author:<tags1/><tags2/>", tus.get(0).getSource().toString());
	}

	@Test
	public void testFieldAndTabAsChar() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setAddTabAsCharacter(true);
		List<ITextUnit> tus = parseTextUnits(getReader("document-field-and-tab.xml"), params);
		assertEquals(1, tus.size());
		assertEquals("Author:\t<tags1/>", tus.get(0).getSource().toString());
	}

	@Test
	public void testEmptyBlock() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-empty.xml"), defaultParams);
		assertEquals(0, tus.size());
	}

	@Test
	public void testSimpleStyles() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-simplestyles.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals(
			"Here’s some text with different styles applied.",
			tus.get(0).getSource().getCodedText().toString());
	}

	@Test
	public void testOverlappingStyles() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-overlapping.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals(
			"This document has overlapping styles.",
			tus.get(0).getSource().getCodedText().toString());
		assertEquals("This <run1>document has <run2>overlapping styles</run2></run1>.",
				tus.get(0).getSource().toString());
	}

	@DataProvider
	public static Object[][] testNoBreakHyphenToCharacterConversionProvider() {
		return new Object[][] {
				{
						new ConditionalParametersBuilder()
								.replaceNoBreakHyphenTag(false)
								.build(),
						new String[] {
								"No break<tags1/>hyphen.<run2>No break<tags3/></run2><tags4/>hyphen.<run5/>"
						}
				},
				{
						new ConditionalParametersBuilder()
								.replaceNoBreakHyphenTag(true)
								.build(),
						new String[] {
								"No break-hyphen.<run1>No break-</run1>-hyphen.<run2>-</run2>"
						}
				},
		};
	}

	@Test
	@UseDataProvider("testNoBreakHyphenToCharacterConversionProvider")
	public void testNoBreakHyphenToCharacterConversion(ConditionalParameters params, String[] expectedTexts) throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-no-break-hyphen.xml"), params);
		assertThat(tus.size(), is(expectedTexts.length));
		for (int i = 0; i < tus.size(); i++) {
			Assert.assertThat(tus.get(i).getSource().toString(), equalTo(expectedTexts[i]));
		}
	}

	@DataProvider
	public static Object[][] testSoftHyphenIgnorationProvider() {
		return new Object[][] {
				{
						new ConditionalParametersBuilder()
								.ignoreSoftHyphenTag(false)
								.build(),
						new String[] {
								"This sentence needs to be long enough to cause some kind of line br<tags1/>eaking.<run2>This sentence needs to be long enough to cause some kind of line br<tags3/></run2><tags4/>eaking.<run5/>"
						}
				},
				{
						new ConditionalParametersBuilder()
								.ignoreSoftHyphenTag(true)
								.build(),
						new String[] {
								"This sentence needs to be long enough to cause some kind of line breaking.<run1>This sentence needs to be long enough to cause some kind of line br</run1>eaking.<run2/>"
						}
				},
		};
	}

	@Test
	@UseDataProvider("testSoftHyphenIgnorationProvider")
	public void testSoftHyphenIgnoration(ConditionalParameters params, String[] expectedTexts) throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-soft-hyphen.xml"), params);
		assertThat(tus.size(), is(expectedTexts.length));
		for (int i = 0; i < tus.size(); i++) {
			Assert.assertThat(tus.get(i).getSource().toString(), equalTo(expectedTexts[i]));
		}
	}

	@DataProvider
	public static Object[][] testLineBreakToCharacterConversionProvider() {
		return new Object[][] {
				{
						new ConditionalParametersBuilder()
								.addLineSeparatorCharacter(false)
								.build(),
						new String[] {
								"Line<tags1/> break.<run2>Line<tags3/></run2><tags4/> break.<run5/>",
								"Carriage<tags1/> return.<run2>Carriage<tags3/></run2><tags4/> return.<run5/>"
						}
				},
				{
						new ConditionalParametersBuilder()
								.addLineSeparatorCharacter(true)
								.build(),
						new String[] {
								"Line\n break.<run1>Line\n</run1>\n break.<run2>\n</run2>",
								"Carriage\n return.<run1>Carriage\n</run1>\n return.<run2>\n</run2>"
						}
				},
		};
	}

	@Test
	@UseDataProvider("testLineBreakToCharacterConversionProvider")
	public void testLineBreakToCharacterConversion(ConditionalParameters params, String[] expectedTexts) throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-br.xml"), params);
		assertThat(tus.size(), is(expectedTexts.length));
		for (int i = 0; i < tus.size(); i++) {
			Assert.assertThat(tus.get(i).getSource().toString(), equalTo(expectedTexts[i]));
		}
	}

	@DataProvider
	public static Object[][] testComplexScriptTagSkippingProvider() {
		return new Object[][] {
				{
						new ConditionalParametersBuilder()
								.cleanupAggressively(false)
								.build(),
						new String[] {
								"<run1>The <run2>garbage</run2><run3> disposal</run3> issue is a real headache to </run1>"
						}
				},
				{
						new ConditionalParametersBuilder()
								.cleanupAggressively(true)
								.build(),
						new String[] {
								"<run1>The garbage disposal issue is a real headache to </run1>"
						}
				},
		};
	}

	@Test
	@UseDataProvider("testComplexScriptTagSkippingProvider")
	public void testComplexScriptTagSkipping(ConditionalParameters params, String[] expectedTexts) throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-complex-script-skip.xml"), params);
		assertThat(tus.size(), is(expectedTexts.length));
		for (int i = 0; i < tus.size(); i++) {
			Assert.assertThat(tus.get(i).getSource().toString(), equalTo(expectedTexts[i]));
		}
	}

	@Test
	public void testComplexStyles() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-complexstyles.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals(
			"This sentence contains very complex styling.",
			tus.get(0).getSource().getCodedText().toString());
		assertEquals("This <run1>sentence <run2>contains</run2></run1><run3> <run4>very</run4></run3> complex <run5>styling.</run5>",
				tus.get(0).getSource().toString());
	}

	@Test
	public void testComplexStyles2() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-complexstyles2.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals(
				"This sentence contains some more complex styling.",
				tus.get(0).getSource().getCodedText().toString());
		assertEquals("<run1>This <run2>sentence</run2> <run3>contains</run3> <run4>some</run4> more</run1> complex styling.",
				tus.get(0).getSource().toString());
	}

	@Test
	public void testHyperlink() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-hyperlink.xml"), defaultParams);
		assertEquals(5, tus.size());
		assertEquals(
				"\uE101\uE110This contains other \uE101\uE111\uE101\uE112hyperlink 1\uE102\uE113\uE101\uE114hyperlink 2\uE102\uE115\uE102\uE116\uE102\uE117\uE101\uE118.\uE102\uE119",
				tus.get(0).getSource().getCodedText());
		assertEquals("<run1>This contains other <hyperlink2><run3>hyperlink 1</run3><run4>hyperlink 2</run4></hyperlink2></run1><run5>.</run5>",
				tus.get(0).getSource().toString());
		assertEquals(
				"This contains a \uE101\uE110hyperlink\uE102\uE111.",
				tus.get(1).getSource().getCodedText());
		assertEquals("This contains a <hyperlink1>hyperlink</hyperlink1>.",
				tus.get(1).getSource().toString());
		assertEquals(
				"\uE101\uE110This contains other \uE101\uE111\uE101\uE112hyperlink 1\uE102\uE113\uE101\uE114hyperlink 2\uE102\uE115\uE102\uE116\uE102\uE117.",
				tus.get(2).getSource().getCodedText());
		assertEquals("<run1>This contains other <hyperlink2><run3>hyperlink 1</run3><run4>hyperlink 2</run4></hyperlink2></run1>.",
				tus.get(2).getSource().toString());
		assertEquals(
				"Here’s another hyperlink that \uE101\uE110\uE101\uE111contains \uE101\uE112styled\uE102\uE113 markup\uE102\uE114\uE102\uE115.",
				tus.get(3).getSource().getCodedText());
		assertEquals("Here’s another hyperlink that <hyperlink1><run2>contains <run3>styled</run3> markup</run2></hyperlink1>.",
				tus.get(3).getSource().toString());
		assertEquals(
				"And another one\uE103\uE110.\uE103\uE111\uE101\uE112\uE102\uE113",
				tus.get(4).getSource().getCodedText());
		assertEquals("And another one<tags1/>.<tags2/><run3></run3>",
				tus.get(4).getSource().toString());
	}

	@Test
	public void testTextBoxInAlternateContent() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-textbox.xml"), defaultParams);
		assertEquals(4, tus.size());
		assertEquals("Text Box 1", tus.get(0).getSource().getCodedText());
		assertEquals("This is a text box.", tus.get(1).getSource().getCodedText());
		assertEquals("This is a <run1>text box</run1>.", tus.get(1).getSource().toString());
		assertEquals("This is a text box.", tus.get(2).getSource().getCodedText());
		assertEquals("This is a <run1>text box</run1>.", tus.get(2).getSource().toString());
		assertEquals("This is not in the text box.", tus.get(3).getSource().getCodedText());
		assertEquals("<run1/>This is not in the text box.", tus.get(3).getSource().toString());
		assertTrue(tus.get(0).isReferent());
		assertTrue(tus.get(1).isReferent());
		assertTrue(tus.get(2).isReferent());
		assertFalse(tus.get(3).isReferent());
	}

	@Test
	public void testTextBoxWithNameOptionDisabled() throws Exception {
		ConditionalParameters params = new ConditionalParameters();
		params.setTranslateWordExcludeGraphicMetaData(true);
		List<ITextUnit> tus = parseTextUnits(getReader("document-textbox.xml"), params);
		assertEquals(3, tus.size());
	}

	@Test
	public void testTextBox() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-textboxes.xml"), defaultParams);
		assertEquals(4, tus.size());
		assertEquals("unbelievable", tus.get(0).getSource().toString());
		assertEquals("pig", tus.get(1).getSource().toString());
		assertEquals("This here is a text box.  ", tus.get(2).getSource().toString());
		assertEquals("<run1/><run2/><run3/>Doggy ", tus.get(3).getSource().toString());
		assertTrue(tus.get(0).isReferent());
		assertTrue(tus.get(1).isReferent());
		assertTrue(tus.get(2).isReferent());
		assertFalse(tus.get(3).isReferent());
	}

	@Test
	public void testFindRunAndTextNames() throws Exception {
		Block block = getBlock(getReader("document-complexstyles.xml"), defaultParams);
		assertEquals(Namespaces.WordProcessingML.getURI(), block.getRunName().getNamespaceURI());
		assertEquals("r", block.getRunName().getLocalPart());
		assertEquals(Namespaces.WordProcessingML.getURI(), block.getTextName().getNamespaceURI());
		assertEquals("t", block.getTextName().getLocalPart());
	}

	@Test
	public void testTextpath() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-textpath.xml"), defaultParams);
		assertEquals(4, tus.size());
		assertEquals("Computer science", tus.get(0).getSource().toString());
		assertEquals("Word art is amazing!", tus.get(1).getSource().toString());
		assertEquals("<run1/>", tus.get(2).getSource().toString());
		assertEquals("systematic", tus.get(3).getSource().toString());
	}

	@Test
	public void testSimpleFields() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("footer-fldSimple.xml"), defaultParams);
		// The TU contains only a single code, so it's made non-translatable
		assertEquals(0, tus.size());
	}

	@Test
	public void testFieldCodes() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-instrText.xml"), defaultParams);
		assertEquals(0, tus.size());
	}

	@Test
	public void testSimpleFields2() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("footer-simpleCode2.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals("Date: ", tus.get(0).getSource().getCodedText());
		assertEquals("Date: <tags1/>", tus.get(0).getSource().toString());
	}

	@Test
	public void testMultipleTabs() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-multitab.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals("A<tags1/>B", tus.get(0).getSource().toString());
	}

	@Test
	public void testNoProof() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-noproof.xml"), defaultParams);
		assertEquals("hello Δ1 world", tus.get(0).getSource().toString());
	}

	@Test
	public void testEmptyFootnotes() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("footnotes-empty.xml"), defaultParams);
		assertEquals(0, tus.size());
	}

	@Test
	public void testStyledHyperlink() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-styled-hyperlink.xml"), defaultParams);
		assertEquals(1, tus.size());
		assertEquals("<run1>This is the text with the <hyperlink2>hyperlink</hyperlink2>.</run1>", tus.get(0).getSource().toString());
	}

	@DataProvider
	public static Object[][] testRunHintsAndFontVariationProvider() {
		return new Object[][] {
			{
				new String[] {
					"<run1>T. 1, Paragraph 1: hint - disabled, same fonts themes;" +
							"T. 1: Paragraph 2: hint - disabled, same fonts themes;</run1>",
					"<run1>T. 2: Paragraph 1: hint - enabled, same fonts themes;" +
							"T. 2 Paragraph 2: hint - disabled, same fonts themes;</run1>",
					"<run1>T. 3 Paragraph 1: hint - disabled, same fonts themes;" +
							"T. 3: Paragraph 2: hint - enabled, same fonts themes;</run1>",
					"<run1>T. 4: Paragraph 1: hint - enabled, same to the next run, same fonts themes;" +
							"T. 4: Paragraph 2: hint - enabled, same to the previous run, same fonts themes;</run1>",
					"<run1>T. 5: Paragraph 1: hint - enabled, different to the next run, same fonts themes;" +
							"T. 5: Paragraph 2: hint - enabled, different to the previous run, same fonts themes;</run1>",
					"<run1>T. 6: Paragraph 1: hint - enabled, different to the next run, different fonts themes;</run1>" +
							"<run2>T. 6: Paragraph 2: hint - enabled, different to the previous run, different fonts themes;</run2>",
					"<run1>T. 7: Paragraph 1: hint - disabled, different to the next run, different fonts themes;</run1>" +
							"<run2>T. 7: Paragraph 2: hint - disabled, different to the previous run, different fonts themes;</run2>",
					"T. 8: Paragraph 1: hint - disabled, different to the next run, no fonts themes;<run1>T. 8: Paragraph 2: hint - disabled, different to the previous run, present font theme;</run1>"
				}
			}
		};
	}

	@Test
	@UseDataProvider("testRunHintsAndFontVariationProvider")
	public void testRunHintsAndFontVariations(String[] expectedTexts) throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-hint.xml"), defaultParams);
		checkExpected(tus, expectedTexts);
	}

	@Test
	public void testFieldSimple2() throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("footer-fldSimple2.xml"), defaultParams);
		assertEquals(0, tus.size());
	}

	@DataProvider
	public static Object[][] testVanishRunPropertyDataProvider() {
		return new Object[][]{
				{
						new String[]{
								"Here is the <run1/>message written by the hand of Jeremiah.",
								"Here is the message of Isaiah.",
								"<run1>Visible 1.</run1>",
								"<run1>Visible 2.</run1>",
								"<run1>Visible 3.</run1>",
								"Visible 4.",
						}
				}
		};
	}

	@Test
	@UseDataProvider("testVanishRunPropertyDataProvider")
	public void testVanishRunProperty(String[] expectedTexts) throws Exception {
		List<ITextUnit> textUnits = parseTextUnits(getReader("document-hidden.xml"), defaultParams);
		checkExpected(textUnits, expectedTexts);
	}

	@DataProvider
	public static Object[][] testTableTusProvider() {
		return new Object[][] {
			{
				new String[] {
					"Text00",
					"Text11",
					"Text11:00",
					"Text11:11",
					"Text12"
				}
			}
		};
	}

	@Test
	@UseDataProvider("testTableTusProvider")
	public void testTableTus(String[] expectedTexts) throws Exception {
		List<ITextUnit> tus = parseTextUnits(getReader("document-bidi-table-properties-2.xml"), defaultParams);
		checkExpected(tus, expectedTexts);
	}

	private void dump(List<ITextUnit> tus) {
		for (ITextUnit tu : tus) {
			//System.out.println(tu.getSource().getCodedText());
			System.out.println(tu.getSource().toString());
		}
	}

	private void checkExpected(List<ITextUnit> tus, String[] expectedTexts) {
		assertThat(tus.size(), is(expectedTexts.length));
		for (int i = 0; i < tus.size(); i++) {
			Assert.assertThat(tus.get(i).getSource().toString(), equalTo(expectedTexts[i]));
		}
	}

	private XMLEventReader getReader(String resource) throws Exception {
		return factories.getInputFactory().createXMLEventReader(
				new InputStreamReader(getClass().getResourceAsStream("/parts/block/" + resource),
						StandardCharsets.UTF_8));
	}
	private List<ITextUnit> parseTextUnits(XMLEventReader xmlReader, ConditionalParameters params) throws XMLStreamException {
		List<ITextUnit> tus = new ArrayList<>();
		IdGenerator textUnitId = new IdGenerator("root", "tu");
		// XXX This code is a little redundant with code in StyledTextPartHandler
		while (xmlReader.hasNext()) {
			XMLEvent e = xmlReader.nextEvent();
			if (isParagraphStartEvent(e)) {
				Block block = new BlockParser(e.asStartElement(), xmlReader,
											  factories.getEventFactory(), params, new IdGenerator(null), StyleDefinitions.emptyStyleDefinitions()).parse();
				BlockTextUnitMapper mapper = new BlockTextUnitMapper(block, textUnitId);
				tus.addAll(mapper.getTextUnits());
			}
		}
		return tus;
	}
	private Block getBlock(XMLEventReader xmlReader, ConditionalParameters params) throws XMLStreamException {
		while (xmlReader.hasNext()) {
			XMLEvent e = xmlReader.nextEvent();
			if (isParagraphStartEvent(e)) {
				return new BlockParser(e.asStartElement(), xmlReader,
											  factories.getEventFactory(), params, new IdGenerator(null), StyleDefinitions.emptyStyleDefinitions()).parse();
			}
		}
		throw new IllegalStateException();
	}
}
