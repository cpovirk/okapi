package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLStreamException;

abstract class DocumentType {
	private OpenXMLZipFile zipFile;
	private ConditionalParameters params;
	private StyleDefinitions styleDefinitions;

	public DocumentType(OpenXMLZipFile zipFile, ConditionalParameters params) {
		this.zipFile = zipFile;
		this.params = params;
	}

	protected OpenXMLZipFile getZipFile() {
		return zipFile;
	}

	protected ConditionalParameters getParams() {
		return params;
	}

	protected StyleDefinitions getStyleDefinitions() {
		return styleDefinitions;
	}

	protected void setStyleDefinitions(StyleDefinitions styleDefinitions) {
		this.styleDefinitions = styleDefinitions;
	}

	abstract OpenXMLPartHandler getHandlerForFile(ZipEntry entry, String mediaType);

	abstract void initialize() throws IOException, XMLStreamException;

	abstract boolean isClarifiablePart(String contentType);

	abstract boolean isStyledTextPart(String entryName, String type);

	/**
	 * Return the zip file entries for this document in the order they should be processed.
	 * @return the zip file entries
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	abstract Enumeration<? extends ZipEntry> getZipFileEntries()
		 throws IOException, XMLStreamException;
}
