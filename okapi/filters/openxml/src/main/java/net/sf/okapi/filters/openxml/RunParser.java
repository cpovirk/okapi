package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.TextFragment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static net.sf.okapi.filters.openxml.AttributeStripper.RevisionAttributeStripper.stripRunRevisionAttributes;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.skipElementEvents;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createGeneralMarkupComponent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.CACHED_PAGE_BREAK;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_REGULAR_HYPHEN_VALUE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_TEXT;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.addChunksToList;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.hasPreserveWhitespace;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isComplexCodeEnd;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isComplexCodeStart;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isGraphicsProperty;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isLineBreakStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isNoBreakHyphenStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isParagraphStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isRunPropsStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isSoftHyphenStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTabStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTextPath;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTextStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isWhitespace;

class RunParser {
	private static final Logger LOGGER = LoggerFactory.getLogger(RunParser.class);

	private StartElement startEvent;
	private XMLEventReader events;
	private XMLEventFactory eventFactory;
	private ConditionalParameters params;
	private StyleDefinitions styleDefinitions;
	private EndElement endEvent;
	private RunProperties runProperties = RunProperties.emptyRunProperties();
	private List<Chunk> runBodyChunks = new ArrayList<>();
	private List<XMLEvent> currentMarkupChunk = new ArrayList<>();
	private QName textName = null;
	private boolean isTextPreservingWhitespace = false;
	private StringBuilder textContent = new StringBuilder();
	private boolean hadAnyText = false, hadNonWhitespaceText = false;
	private List<Textual> nestedTextualItems = new ArrayList<>();
	private boolean containsNestedItems = false;
	private IdGenerator nestedTextualIds;
	private int complexCodeDepth = 0;
	private boolean hasComplexCodes;
	private String runStyle;

	RunParser(StartElement startEvent, XMLEventReader events, XMLEventFactory eventFactory,
					 IdGenerator nestedTextualIds, ConditionalParameters params, StyleDefinitions styleDefinitions) {
		this.startEvent = startEvent;
		this.events = events;
		this.eventFactory = eventFactory;
		this.nestedTextualIds = nestedTextualIds;
		this.params = params;
		this.styleDefinitions = styleDefinitions;
	}

	RunParser parse() throws XMLStreamException {
		log("startRun: " + startEvent);
		startEvent = stripRunRevisionAttributes(eventFactory, startEvent);
		// rPr is either the first child, or not present (section 17.3.2)
		XMLEvent firstChild = events.nextTag();
		if (isRunPropsStartEvent(firstChild)) {
			processRunProps(firstChild.asStartElement(), events);
		}
		else if (isEndElement(firstChild, startEvent)) {
			// Empty run!
			return endRun(firstChild.asEndElement());
		}
		else {
			// No properties section
			processRunBody(firstChild, events);
		}
		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			log("processRun: " + e);
			if (isEndElement(e, startEvent)) {
				return endRun(e.asEndElement());
			}
			else {
				// Handle non-properties (run body) content
				processRunBody(e, events);
			}
		}
		throw new IllegalStateException("Invalid content? Unterminated run");
	}

	QName getTextName() {
		return textName;
	}

	private void processRunProps(StartElement startEvent, XMLEventReader events) throws XMLStreamException {
		runProperties = new RunPropertiesParser(startEvent, events, params, eventFactory).parse();

		RunProperty.RunStyleProperty runStyleProperty = runProperties.getRunStyleProperty();
		runStyle = null == runStyleProperty
				? null
				: runStyleProperty.getRunStyle();
	}

	private RunParser endRun(EndElement e) throws XMLStreamException {
		flushText();
		flushMarkupChunk();
		this.endEvent = e;
		// XXX This is pretty hacky.
		// Recalculate the properties now that consolidation has already happened.
		// This is required in order to properly handle the aggressive-mode trimming
		// of the vertAlign property, which is only done if there's no text in the
		// run.  Whether or not text is present can only be correctly calculated
		// -after- other run merging has already taken place.
		if (!hadNonWhitespaceText && params.getCleanupAggressively()) {
			runProperties = RunProperties.copiedRunProperties(runProperties, true, false, false);
		}
		return this;
	}

	Run build() throws XMLStreamException {
		return new Run(startEvent, endEvent,
					   runProperties,
					   runBodyChunks, nestedTextualItems);
	}

	@Override
	public String toString() {
		try {
			return "RunParser for " + build().toString();
		}
		catch (XMLStreamException e) {
			return "RunParser (" + e.getMessage() + ")";
		}
	}

	boolean canMerge(RunParser otherRunParser, String paragraphStyle) {
		// Merging runs in the math namespace can sometimes corrupt formulas,
		// so don't do it.
		if (Namespaces.Math.containsName(startEvent.getName())) {
			return false;
		}
		// XXX Don't merge runs that have nested blocks, to avoid having to go
		// back and renumber the references in the skeleton. I should probably
		// fix this at some point.  Note that we check for the existence of -any-
		// nested block, not just ones with text.
		// The reason for this pre-caution is because when we merge runs, we
		// re-parse the xml events.  However, it doesn't cover all the cases.
		// We might be able to remove this restriction if we could clean up the
		// way the run body events are parsed during merging.
		if (containsNestedItems || otherRunParser.containsNestedItems) {
			return false;
		}
		// Don't merge stuff involving complex codes
		if (hasComplexCodes || otherRunParser.hasComplexCodes) {
			return false;
		}
		// Runs that have no properties specified can always be merged
		if (runProperties.count() == 0 && otherRunParser.runProperties.count() == 0) {
			return true;
		}

		return canRunPropertiesBeMerged(otherRunParser, paragraphStyle);
	}

	private boolean canRunPropertiesBeMerged(RunParser otherRunParser, String paragraphStyle) {
		if (runProperties == otherRunParser.runProperties) {
			return true;
		}

		RunProperties currentProperties = styleDefinitions.getCombinedRunProperties(paragraphStyle, runStyle, runProperties);
		RunProperties otherProperties = styleDefinitions.getCombinedRunProperties(paragraphStyle, otherRunParser.runStyle, otherRunParser.runProperties);

		if (currentProperties.count() != otherProperties.count()) {
			return false;
		}

		int numberOfMatchedProperties = 0;

		for (RunProperty currentProperty : currentProperties.getProperties()) {
			QName currentPropertyStartElementName = currentProperty.getName();

			for (RunProperty otherProperty : otherProperties.getProperties()) {
				QName otherPropertyStartElementName = otherProperty.getName();

				if (!currentPropertyStartElementName.equals(otherPropertyStartElementName)) {
					continue;
				}

				if (currentProperty instanceof MergeableRunProperty && otherProperty instanceof MergeableRunProperty) {
					if (!((MergeableRunProperty) currentProperty).canBeMerged((MergeableRunProperty) otherProperty)) {
						return false;
					}
				} else {
					if (!currentProperty.canBeReplaced(otherProperty)) {
						return false;
					}
				}
				numberOfMatchedProperties++;
				break;
			}
		}

		if (numberOfMatchedProperties < currentProperties.count()) {
			return false;
		}

		return true;
	}

	/**
	 * Merge the property body.  This is something of a mess, since
	 * this may cause tetris-style collapsing of consecutive text across
	 * the two runs. To handle this correctly, we concatenate the two run
	 * bodies, then re-process them.
	 */
	void merge(RunParser other) throws XMLStreamException {
		mergeRunProperties(other);
		List<XMLEvent> newBodyEvents = new ArrayList<>();
		addChunksToList(newBodyEvents, runBodyChunks);
		isTextPreservingWhitespace |= other.isTextPreservingWhitespace;
		addChunksToList(newBodyEvents, other.runBodyChunks);
		runBodyChunks.clear();
		XMLEventReader events = new XMLListEventReader(newBodyEvents);
		while (events.hasNext()) {
			addRunBody(events.nextEvent(), events);
		}
		// Flush any outstanding buffers
		flushText();
		flushMarkupChunk();
	}

	private void mergeRunProperties(RunParser otherRunParser) {
		// try to reduce the set of properties
		if (0 == runProperties.count() && 0 == otherRunParser.runProperties.count()) {
			return;
		}

		List<RunProperty> mergeableRunProperties = runProperties.getMergeableRunProperties();

		if (0 == mergeableRunProperties.size()) {
			runProperties = otherRunParser.runProperties;
			return;
		}

		List<RunProperty> otherMergeableRunProperties = otherRunParser.runProperties.getMergeableRunProperties();

		if (0 == otherMergeableRunProperties.size()) {
			return;
		}

		if (mergeableRunProperties.size() >= otherMergeableRunProperties.size()) {
			List<RunProperty> remainedOtherMergeableRunProperties = mergeMergeableRunProperties(mergeableRunProperties, otherMergeableRunProperties);
			runProperties.getProperties().addAll(remainedOtherMergeableRunProperties);
			return;
		}

		List<RunProperty> remainedOtherMergeableRunProperties = mergeMergeableRunProperties(otherMergeableRunProperties, mergeableRunProperties);
		otherRunParser.runProperties.getProperties().addAll(remainedOtherMergeableRunProperties);
		runProperties = otherRunParser.runProperties;
	}

	private List<RunProperty> mergeMergeableRunProperties(List<RunProperty> mergeableRunProperties, List<RunProperty> otherMergeableRunProperties) {
		List<RunProperty> remainedOtherMergeableRunProperties = new ArrayList<>(otherMergeableRunProperties);

		for (RunProperty runProperty : mergeableRunProperties) {
			QName currentPropertyStartElementName = runProperty.getName();

			Iterator<RunProperty> remainedOtherMergeableRunPropertyIterator = remainedOtherMergeableRunProperties.iterator();

			while (remainedOtherMergeableRunPropertyIterator.hasNext()) {
				RunProperty otherRunProperty = remainedOtherMergeableRunPropertyIterator.next();
				QName otherPropertyStartElementName = otherRunProperty.getName();

				if (!currentPropertyStartElementName.equals(otherPropertyStartElementName)) {
					continue;
				}

				((MergeableRunProperty) runProperty).merge((MergeableRunProperty) otherRunProperty);
				remainedOtherMergeableRunPropertyIterator.remove();
				break;
			}
		}

		return remainedOtherMergeableRunProperties;
	}

	private void processRunBody(XMLEvent e, XMLEventReader events) throws XMLStreamException {
		if (isParagraphStartEvent(e)) {
			log("Nested block start event: " + e);
			flushText();
			BlockParser nestedBlockParser = new BlockParser(e.asStartElement(),
													events, eventFactory, params, nestedTextualIds, styleDefinitions);
			Block nested = nestedBlockParser.parse();
			containsNestedItems = true;
			if (nested.hasTranslatableRunContent()) {
				// Create a reference to mark the location of the nested block
				addToMarkupChunk(eventFactory.createCharacters(
						TextFragment.makeRefMarker(nestedTextualIds.createId())));
				nestedTextualItems.add(nested);
			}
			else {
				// Empty block, we don't need to expose it after all
				for (XMLEvent nestedEvent : nested.getEvents()) {
					addToMarkupChunk(nestedEvent);
				}
				// However, we do need to preserve anything it references that's translatable
				for (Chunk chunk : nested.getChunks()) {
					if (chunk instanceof Run) {
						nestedTextualItems.addAll(((Run)chunk).getNestedTextualItems());
					}
					else if (chunk instanceof RunContainer) {
						for (Run run : ((RunContainer)chunk).getRuns()) {
							nestedTextualItems.addAll(run.getNestedTextualItems());
						}
					}
				}
			}
		}
		// XXX I need to make sure I don't try to merge this thing
		else if (isComplexCodeStart(e)) {
			complexCodeDepth++;
			hasComplexCodes = true;
			addToMarkupChunk(e);
			// Treat ~everything~ as markup until we are completely out
			// of codes.  Note that codes can nest, so we keep a depth count.
			while (events.hasNext()) {
				e = (XMLEvent)events.next();
				addToMarkupChunk(e);
				if (isComplexCodeEnd(e)) {
					if (--complexCodeDepth <= 0) {
						break;
					}
				}
				else if (isComplexCodeStart(e)) {
					complexCodeDepth++;
				}
			}
		}
		else {
			addRunBody(processTranslatableAttributes(e), events);
		}
	}

	// translatable attributes:
	// wp:docPr/@name  if that option isn't set
	// v:textpath/@string
	private XMLEvent processTranslatableAttributes(XMLEvent e) {
		if (!e.isStartElement()) return e;
		StartElement startEl = e.asStartElement();
		// I will need to
		// - extract translatable attribute
		// - create a new start event with all the attributes except for that one, which is replaced
		if (isGraphicsProperty(startEl) && !params.getTranslateWordExcludeGraphicMetaData()) {
			startEl = processTranslatableAttribute(startEl, "name");
		}
		else if (isTextPath(startEl)) {
			startEl = processTranslatableAttribute(startEl, "string");
		}
		return startEl;
	}

	private StartElement processTranslatableAttribute(StartElement startEl, String attrName) {
		List<Attribute> newAttrs = new ArrayList<>();
		Iterator<?> it = startEl.getAttributes();
		boolean dirty = false;
		while (it.hasNext()) {
			Attribute a = (Attribute)it.next();
			if (a.getName().getLocalPart().equals(attrName)) {
				containsNestedItems = true;
				nestedTextualItems.add(new UnstyledText(a.getValue()));
				newAttrs.add(eventFactory.createAttribute(a.getName(),
						TextFragment.makeRefMarker(nestedTextualIds.createId())));
				dirty = true;
			}
			else {
				newAttrs.add(a);
			}
		}
		return dirty ?
			eventFactory.createStartElement(startEl.getName(), newAttrs.iterator(), startEl.getNamespaces()) :
			startEl;
	}

	private void addRunBody(XMLEvent e, XMLEventReader events) throws XMLStreamException {
		if (isTextStartEvent(e)) {
			flushMarkupChunk();
			processText(e.asStartElement(), events);
		}
		else if (params.getAddTabAsCharacter() && isTabStartEvent(e)) {
			flushMarkupChunk();
			addRawText("\t", e.asStartElement(), events);
		}
		else if (params.getAddLineSeparatorCharacter() && isLineBreakStartEvent(e)) {
			flushMarkupChunk();
			addRawText("\n", e.asStartElement(), events);
		}
		else if (e.isStartElement() && isStrippableRunBodyElement(e.asStartElement())) {
			// Consume to the end of the element
			skipElementEvents(events, e.asStartElement());
		}
		else if (params.getReplaceNoBreakHyphenTag() && isNoBreakHyphenStartEvent(e)){
			flushMarkupChunk();
			addRawText(LOCAL_REGULAR_HYPHEN_VALUE, e.asStartElement(), events);
		}
		else if (params.getIgnoreSoftHyphenTag() && isSoftHyphenStartEvent(e)){
			// Ignore soft hyphens
			skipElementEvents(events, e.asStartElement());
		}
		// Real text should have been handled above.  Most whitespace is ignorable,
		// but if we're in a preserve-whitespace section, we need to save it (eg
		// for w:instrText, which isn't translatable but needs to be preserved).
		else if (!isWhitespace(e) || inPreservingWhitespaceElement()) {
			flushText();
			isTextPreservingWhitespace = false;
			addToMarkupChunk(e);
		}
	}

	/**
	 * Handle cases like <w:instrText> -- non-text elements that we may need
	 * to obey xml:space="preserve" on.
	 */
	private boolean inPreservingWhitespaceElement() {
		// Look only at the most recent element on the stack
		if (currentMarkupChunk.size() > 0) {
			XMLEvent e = currentMarkupChunk.get(currentMarkupChunk.size() - 1);
			if (e instanceof StartElement && XMLEventHelpers.hasPreserveWhitespace(e.asStartElement())) {
				return true;
			}
		}
		return false;
	}

	private void processText(StartElement startEvent, XMLEventReader events) throws XMLStreamException {
		hadAnyText = true;
		// Merge the preserve whitespace flag
		isTextPreservingWhitespace = isTextPreservingWhitespace ? true :
				hasPreserveWhitespace(startEvent);
		if (textName == null) {
			textName = startEvent.getName();
		}
		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			if (isEndElement(e, startEvent)) {
				return;
			}
			else if (e.isCharacters()) {
				String text = e.asCharacters().getData();
				if (text.trim().length() > 0) {
					hadNonWhitespaceText = true;
				}
				textContent.append(text);
			}
		}
	}

	private void flushText() {
		// It seems like there may be a bug where presml runs need to have
		// an empty <a:t/> at a minimum.
		if (hadAnyText) {
			runBodyChunks.add(new Run.RunText(createTextStart(),
									eventFactory.createCharacters(textContent.toString()),
									eventFactory.createEndElement(textName, null)));
			textContent.setLength(0);
			hadAnyText = false;
		}
	}

	private StartElement createTextStart() {
		return eventFactory.createStartElement(textName,
				// DrawingML <a:t> does not use the xml:space="preserve" attribute
				isTextPreservingWhitespace && !Namespaces.DrawingML.containsName(textName) ?
						Collections.singleton(
								eventFactory.createAttribute("xml", Namespaces.XML.getURI(), "space", "preserve"))
								.iterator() : null,
						null);
	}

	private boolean isStrippableRunBodyElement(StartElement el) {
		return el.getName().equals(CACHED_PAGE_BREAK);
	}

	private void addRawText(String text, StartElement startEl, XMLEventReader events) throws XMLStreamException {
		hadAnyText = true;
		textContent.append(text);
		if (startEl != null) {
			skipElementEvents(events, startEl);
		}
		if (textName == null) {
			textName = new QName(startEl.getName().getNamespaceURI(), LOCAL_TEXT, startEl.getName().getPrefix());
		}
	}

	private void addToMarkupChunk(XMLEvent event) {
		currentMarkupChunk.add(event);
	}

	private void flushMarkupChunk() {
		if (currentMarkupChunk.size() > 0) {
			runBodyChunks.add(new Run.RunMarkup().addComponent(createGeneralMarkupComponent(currentMarkupChunk)));
			currentMarkupChunk = new ArrayList<>();
		}
	}

	private void log(String s) {
		LOGGER.debug(s);
	}

	/**
	 * Wrap an event iterator as a true reader; this lets us replay strings
	 * of events during merge.
	 */
	class XMLListEventReader implements XMLEventReader {
		private Iterator<XMLEvent> events;
		XMLListEventReader(Iterable<XMLEvent> events) {
			this.events = events.iterator();
		}
		@Override public boolean hasNext() {
			return events.hasNext();
		}
		@Override public XMLEvent nextEvent() {
			return events.next();
		}
		@Override public void remove() {
			events.remove();
		}
		@Override public XMLEvent nextTag() throws XMLStreamException {
			for (XMLEvent e = nextEvent(); e != null; e = nextEvent()) {
				if (e.isStartElement() || e.isEndElement()) {
					return e;
				}
				else if (!isWhitespace(e)) {
					throw new IllegalStateException("Unexpected event: " + e);
				}
			}
			return null;
		}
		@Override public XMLEvent peek() throws XMLStreamException {
			throw new UnsupportedOperationException();
		}
		@Override public String getElementText() throws XMLStreamException {
			throw new UnsupportedOperationException();
		}
		@Override public Object getProperty(String name) throws IllegalArgumentException {
			throw new UnsupportedOperationException();
		}
		@Override public void close() throws XMLStreamException {
		}
		@Override public Object next() {
			return events.next();
		}
	}
}
