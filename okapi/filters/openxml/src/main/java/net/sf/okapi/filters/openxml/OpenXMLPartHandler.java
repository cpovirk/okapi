package net.sf.okapi.filters.openxml;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;

public interface OpenXMLPartHandler {
	/**
	 * Open this part and perform any initial processing.  Return the
	 * first event for this part.
	 * @param docId document identifier
	 * @param subDocId sub-document identifier
	 * @param srcLang the locale of the source
	 * @return first event for this part.
	 * @throws IOException if any problem is encountered
	 */
	Event open(String docId, String subDocId, LocaleId srcLang) throws IOException, XMLStreamException;

	boolean hasNext();

	Event next();

	void close();

	void logEvent(Event e);
}
