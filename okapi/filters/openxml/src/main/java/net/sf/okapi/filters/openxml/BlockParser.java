package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.IdGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

import static net.sf.okapi.filters.openxml.AttributeStripper.RevisionAttributeStripper.stripParagraphRevisionAttributes;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralCrossStructureSkippableElement.BOOKMARK_END;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralCrossStructureSkippableElement.BOOKMARK_START;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.isDeletedRunContentStartElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.isInsertedRunContentElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.isProofingErrorStartElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.skipElementEvents;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.PARAGRAPH_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTY_DELETED_PARAGRAPH_MARK;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTY_INSERTED_PARAGRAPH_MARK;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createEndMarkupComponent;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createGeneralMarkupComponent;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createStartMarkupComponent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_VAL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.gatherEvents;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isParagraphPropertiesStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isRunContainerStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isRunStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isSimpleFieldStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isWhitespace;

/**
 * Given an event stream and a block start element, this will parse and return
 * the block object.
 */
class BlockParser {
	private static final Logger LOGGER = LoggerFactory.getLogger(BlockParser.class);

	private StartElement blockStartEvent;
	private XMLEventReader events;
	private XMLEventFactory eventFactory;
	private ConditionalParameters params;
	private IdGenerator nestedBlockIdGenerator;
	private StyleDefinitions styleDefinitions;
	private ElementSkipper generalElementSkipper;
	private ElementSkipper bookmarkElementSkipper;

	BlockParser(StartElement startEvent, XMLEventReader events, XMLEventFactory eventFactory,
					   ConditionalParameters params, IdGenerator nestedBlockIdGenerator, StyleDefinitions styleDefinitions) {
		this.blockStartEvent = startEvent;
		this.events = events;
		this.eventFactory = eventFactory;
		this.params = params;
		this.nestedBlockIdGenerator = nestedBlockIdGenerator;
		this.styleDefinitions = styleDefinitions;

		generalElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
				RUN_PROPERTY_INSERTED_PARAGRAPH_MARK,
				RUN_PROPERTY_DELETED_PARAGRAPH_MARK,
				PARAGRAPH_PROPERTIES_CHANGE,
				RUN_PROPERTIES_CHANGE);

		bookmarkElementSkipper = ElementSkipperFactory.createBookmarkElementSkipper(
				BOOKMARK_START,
				BOOKMARK_END);
	}

	static class RunMerger {
		private String paragraphStyle;
		private RunParser previousRunParser;
		private List<Run> completedRuns = new ArrayList<>();

		void setParagraphStyle(String paragraphStyle) {
			this.paragraphStyle = paragraphStyle;
		}

		void add(RunParser rp) throws XMLStreamException {
			if (previousRunParser != null) {
				if (previousRunParser.canMerge(rp, paragraphStyle)) {
					previousRunParser.merge(rp);
				}
				else {
					completedRuns.add(previousRunParser.build());
					previousRunParser = rp;
				}
			}
			else {
				previousRunParser = rp;
			}
		}

		List<Run> getRuns() throws XMLStreamException {
			if (previousRunParser != null) {
				completedRuns.add(previousRunParser.build());
				previousRunParser = null;
			}
			return completedRuns;
		}

		void reset() {
			completedRuns.clear();
			previousRunParser = null;
		}
	}

	private void addRunsToBuilder(BlockBuilder builder, RunMerger runMerger) throws XMLStreamException {
		for (Run run : runMerger.getRuns()) {
			builder.addChunk(run);
		}
		runMerger.reset();
	}

	private void parseRunContainer(BlockBuilder builder, StartElement runContainerStart) throws XMLStreamException {
		RunMerger runMerger = new RunMerger();
		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			// Check for end of container
			if (e.isEndElement() && runContainerStart.getName().equals(e.asEndElement().getName())) {
				builder.addChunk(new RunContainer(runContainerStart, e.asEndElement(), runMerger.getRuns()));
				return;
			}
			if (isRunStartEvent(e)) {
				processRun(builder, runMerger, e.asStartElement());
			}
			// XXX Currently we're stripping everything else, because I'm not sure how much
			// other block stuff can happen inside a hyperlink.  This is quite possibly incorrect --
			// the RunContainer and block parsing code may have to merge further.
		}
		throw new IllegalStateException("Invalid content? Unterminated run container");
	}

	Block parse() throws XMLStreamException {
		BlockBuilder builder = new BlockBuilder();
		log("startBlock: " + blockStartEvent);
		builder.addMarkupComponent(createStartMarkupComponent(eventFactory, stripParagraphRevisionAttributes(eventFactory, blockStartEvent)));
		RunMerger runMerger = new RunMerger();
		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			if (isParagraphPropertiesStartEvent(e)) {
				MarkupComponent blockProperties = MarkupComponentParser.parseBlockProperties(events, eventFactory, e.asStartElement(), generalElementSkipper);
				builder.addMarkupComponent(blockProperties);

				String paragraphStyle = getParagraphStyle(((BlockProperties) blockProperties).getParagraphStyleProperty());
				runMerger.setParagraphStyle(paragraphStyle);
				builder.setIsHidden(hasToBeHidden(paragraphStyle));
			}
			else if (isRunStartEvent(e)) {
				processRun(builder, runMerger, e.asStartElement());
			}
			else if (isRunContainerStartEvent(e)) {
				StartElement runContainerStart = e.asStartElement();
				// Flush previous run, if any
				addRunsToBuilder(builder, runMerger);
				// Build the run container and add it as a single chunk
				parseRunContainer(builder, runContainerStart);
			}
			else if (isSimpleFieldStartEvent(e)) {
				addRunsToBuilder(builder, runMerger);
				for (XMLEvent fldEvent : gatherEvents(e.asStartElement(), events)) {
					builder.addEvent(fldEvent);
				}
				// Flush it so it will all end up as a single code with nothing else
				builder.flushMarkup();
			}
			else {
				if (processSkippableElements(e)) {
					continue;
				}

				// Trim non-essential whitespace
				if (!isWhitespace(e)) {
					// Flush any outstanding run if there's any markup
					addRunsToBuilder(builder, runMerger);

					// Check for end of block
					if (e.isEndElement() && blockStartEvent.getName().equals(e.asEndElement().getName())) {
						builder.addMarkupComponent(createEndMarkupComponent(e.asEndElement()));
						log("End block: " + e);
						return builder.build();
					} else {
						builder.addEvent(e);
					}
				}
			}
		}
		throw new IllegalStateException("Invalid content? Unterminated paragraph");
	}

	/**
	 * Processes skippable elements and skips found.
	 *
	 * @param event An XML event
	 *
	 * @return {@code true}  - if an element has been skipped
	 *         {@code false} - otherwise
	 *
	 * @throws XMLStreamException
     */
	private boolean processSkippableElements(XMLEvent event) throws XMLStreamException {
		if (isInsertedRunContentElement(event)) {
			return true;
        }
		if (isDeletedRunContentStartElement(event)) {
            skipElementEvents(events, event.asStartElement());
			return true;
        }
		if (isProofingErrorStartElement(event)) {
			skipElementEvents(events, event.asStartElement());
			return true;
        }
		if (event.isStartElement() && bookmarkElementSkipper.isSkippableElement(event.asStartElement())) {
			bookmarkElementSkipper.skipElement(events, event.asStartElement());
			return true;
        }

		return false;
	}

	private void processRun(BlockBuilder builder, RunMerger runMerger, StartElement startEl) throws XMLStreamException {
		RunParser runParser = new RunParser(startEl, events, eventFactory, nestedBlockIdGenerator, params, styleDefinitions).parse();
		builder.setRunName(startEl.getName());
		builder.setTextName(runParser.getTextName());
		runMerger.add(runParser);
	}

	private String getParagraphStyle(BlockProperty paragraphStyleProperty) {
		if (null == paragraphStyleProperty) {
			return null;
		}

		return getAttributeValue(paragraphStyleProperty.getEvents().get(0).asStartElement(), WPML_VAL);
	}

	private boolean hasToBeHidden(String paragraphStyle) {
		return !params.getTranslateWordHidden()
				&& null != paragraphStyle
				&& params.tsExcludeWordStyles.contains(paragraphStyle);
	}

	private class BlockBuilder {
		private List<Chunk> chunks = new ArrayList<>();
		private List<XMLEvent> currentMarkupComponentEvents = new ArrayList<>();
		private Markup markup = new Block.BlockMarkup();
		private boolean isHidden = false;
		private QName runName, textName;

		BlockBuilder() { }

		void setIsHidden(boolean isHidden) {
			this.isHidden = isHidden;
		}

		void setRunName(QName runName) {
			if (this.runName == null) {
				this.runName = runName;
			}
		}

		void setTextName(QName textName) {
			if (this.textName == null) {
				this.textName = textName;
			}
		}

		private void flushMarkup() {
			if (!currentMarkupComponentEvents.isEmpty()) {
				markup.addComponent(createGeneralMarkupComponent(currentMarkupComponentEvents));
				currentMarkupComponentEvents = new ArrayList<>();
			}
			if (!markup.getComponents().isEmpty()) {
				chunks.add(markup);
				markup = new Block.BlockMarkup();
			}
		}

		void addEvent(XMLEvent event) {
			currentMarkupComponentEvents.add(event);
		}

		void addChunk(Chunk chunk) {
			flushMarkup();
			chunks.add(chunk);
		}

		void addMarkupComponent(MarkupComponent markupComponent) {
			if (!currentMarkupComponentEvents.isEmpty()) {
				markup.addComponent(createGeneralMarkupComponent(currentMarkupComponentEvents));
				currentMarkupComponentEvents = new ArrayList<>();
			}
			markup.addComponent(markupComponent);
		}

		Block build() {
			flushMarkup();
			return new Block(chunks, runName, textName, isHidden);
		}
	}

	private void log(String s) {
		LOGGER.debug(s);
	}
}
