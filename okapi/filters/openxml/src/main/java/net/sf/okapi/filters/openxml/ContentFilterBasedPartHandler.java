package net.sf.okapi.filters.openxml;

import java.io.InputStream;
import java.util.zip.ZipEntry;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.skeleton.GenericSkeleton;
import net.sf.okapi.common.skeleton.ZipSkeleton;

/**
 * Base class for part handlers that wrap an OpenXMLContentFilter.
 */
public abstract class ContentFilterBasedPartHandler implements OpenXMLPartHandler {
	protected OpenXMLZipFile zipFile;
	protected ZipEntry entry;
	protected OpenXMLContentFilter contentFilter;
	protected ConditionalParameters cparams;

	ContentFilterBasedPartHandler(OpenXMLContentFilter contentFilter, ConditionalParameters cparams,
				OpenXMLZipFile zipFile, ZipEntry entry) {
		this.zipFile = zipFile;
		this.entry = entry;
		this.contentFilter = contentFilter;
		this.cparams = cparams;
	}

	@Override
	public boolean hasNext() {
		return contentFilter.hasNext();
	}

	@Override
	public Event next() {
		return contentFilter.next();
	}

	@Override
	public void close() {
		contentFilter.close();
	}

	/**
	 * Open the nested {@link OpenXMLContentFilter} instance on the specified InputStream,
	 * and convert a START_SUBDOCUMENT event for it.
	 * @param is input stream
	 * @param docId document identifier
	 * @param subDocId sub-document identifier
	 * @param srcLang the source language
	 * @return the START_SUBDOCUMENT Event
	 */
	protected Event openContentFilter(InputStream is, String docId, String subDocId, LocaleId srcLang) {
		contentFilter.open(new RawDocument(is, "UTF-8", srcLang));
		Event startDocEvent = contentFilter.next();
		// Change the START_DOCUMENT event to START_SUBDOCUMENT
		StartSubDocument sd = new StartSubDocument(docId, subDocId);
		sd.setName(entry.getName());
		ConditionalParameters clonedParams = cparams.clone();
		clonedParams.nFileType = contentFilter.getParseType();
		sd.setFilterParameters(clonedParams);
		ZipSkeleton skel = new ZipSkeleton((GenericSkeleton)startDocEvent.getStartDocument().getSkeleton(),
										   zipFile.getZip(), entry);
		return new Event(EventType.START_SUBDOCUMENT, sd, skel);
	}

	@Override
	public void logEvent(Event e) {
		contentFilter.displayOneEvent(e);
	}
}
