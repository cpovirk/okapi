package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Representation of a parsed text run.
 */
public class Run implements Block.BlockChunk {
	private StartElement startEvent;
	private EndElement endEvent;
	private RunProperties runProperties;
	private List<Chunk> bodyChunks;
	private List<Textual> nestedTextualItems;

	Run(StartElement startEvent, EndElement endEvent, RunProperties runProperties,
							List<Chunk> bodyChunks, List<Textual> nestedTextualItems) {
		this.startEvent = startEvent;
		this.endEvent = endEvent;
		this.runProperties = runProperties;
		this.bodyChunks = bodyChunks;
		this.nestedTextualItems = nestedTextualItems;
	}

	public RunProperties getProperties() {
		return runProperties;
	}

	public List<Chunk> getBodyChunks() {
		return bodyChunks;
	}

	public List<Textual> getNestedTextualItems() {
		return nestedTextualItems;
	}

	/**
	 * Return true if this run contains translatable (non-hidden) text.
	 */
	public boolean containsText() {
		for (Chunk c : bodyChunks) {
			if (c instanceof RunText) {
				return !getProperties().isHidden();
			}
		}
		return false;
	}

	@Override
	public List<XMLEvent> getEvents() {
		List<XMLEvent> events = new ArrayList<>();
		events.add(startEvent);
		events.addAll(runProperties.getEvents());
		for (XMLEvents chunk : bodyChunks) {
			events.addAll(chunk.getEvents());
		}
		events.add(endEvent);
		return events;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + XMLEventSerializer.serialize(getEvents()) + "]";
	}

	public static class RunText implements RunChunk {
		private StartElement startText;
		private Characters text;
		private EndElement endText;
		RunText(StartElement startText, Characters text, EndElement endText) {
			this.startText = startText;
			this.text = text;
			this.endText = endText;
		}
		public boolean isPreserveSpace() {
			return XMLEventHelpers.hasPreserveWhitespace(startText);
		}
		public String getText() {
			return text.getData();
		}
		@Override
		public List<XMLEvent> getEvents() {
			List<XMLEvent> events = new ArrayList<>(3);
			events.add(startText);
			events.add(text);
			events.add(endText);
			return events;
		}
		@Override
		public String toString() {
			return getClass().getSimpleName() + "[" + XMLEventSerializer.serialize(startText) + "](" + text + ")";
		}
	}

	/**
	 * Marker interface to distinguish XMLEvents implementation that
	 * can be added to a Run as body content.
	 */
	public interface RunChunk extends Chunk { }

	static class RunMarkup extends Markup implements RunChunk { }
}
