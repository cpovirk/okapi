package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Objects;
import java.util.Set;

import static net.sf.okapi.filters.openxml.ElementSkipper.BookmarkElementSkipper.isBookmarkEndElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.BookmarkElementSkipper.isBookmarkStartElement;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_ID;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_NAME;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;

/**
 * Provides an element skipper strategy.
 */
abstract class ElementSkipperStrategy {

    protected Set<String> skippableElementValues;

    protected ElementSkipperStrategy(Set<String> skippableElementValues) {
        this.skippableElementValues = skippableElementValues;
    }

    abstract boolean isSkippableElement(StartElement startElement);

    abstract void skipElement(XMLEventReader eventReader, StartElement startElement) throws XMLStreamException;

    /**
     * Provides a general element skipper strategy.
     */
    static class GeneralElementSkipperStrategy extends ElementSkipperStrategy {

        GeneralElementSkipperStrategy(Set<String> skippableElementValues) {
            super(skippableElementValues);
        }

        static void skipElementEvents(XMLEventReader eventReader, StartElement startElement) throws XMLStreamException {
            while (eventReader.hasNext()) {
                XMLEvent e = eventReader.nextEvent();

                if (isEndElement(e, startElement)) {
                    return;
                }
            }

            throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
        }

        @Override
        boolean isSkippableElement(StartElement startElement) {
            return skippableElementValues.contains(startElement.getName().getLocalPart());
        }

        @Override
        void skipElement(XMLEventReader eventReader, StartElement startElement) throws XMLStreamException {
            skipElementEvents(eventReader, startElement);
        }
    }

    /**
     * Provides a bookmark element skipper strategy.
     */
    static class BookmarkElementSkipperStrategy extends GeneralElementSkipperStrategy {

        private String bookmarkName;
        private String bookmarkId;

        BookmarkElementSkipperStrategy(Set<String> skippableElementValues, String bookmarkName) {
            super(skippableElementValues);
            this.bookmarkName = bookmarkName;
        }

        @Override
        boolean isSkippableElement(StartElement startElement) {
            return isBookmarkStartElement(startElement) && bookmarkName.equals(getAttributeValue(startElement, WPML_NAME))
                    || isBookmarkEndElement(startElement) && Objects.equals(bookmarkId, getAttributeValue(startElement, WPML_ID));
        }

        @Override
        void skipElement(XMLEventReader eventReader, StartElement startElement) throws XMLStreamException {
            super.skipElement(eventReader, startElement);

            if (isBookmarkStartElement(startElement)) {
                bookmarkId = getAttributeValue(startElement, WPML_ID);
                return;
            }

            bookmarkId = null;
        }
    }
}
