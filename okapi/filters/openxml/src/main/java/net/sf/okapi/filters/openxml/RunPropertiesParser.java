package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static net.sf.okapi.filters.openxml.AttributeStripper.GeneralAttributeStripper.stripGeneralAttributes;
import static net.sf.okapi.filters.openxml.AttributeStripper.RevisionAttributeStripper.stripRunRevisionAttributes;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RunPropertySkippableElement.RUN_PROPERTY_LANGUAGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RunPropertySkippableElement.RUN_PROPERTY_NO_SPELLING_OR_GRAMMAR;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.DEFAULT_BOOLEAN_ATTRIBUTE_TRUE_VALUE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_PROPERTY_VANISH;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_VAL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getBooleanAttributeValue;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isWhitespace;

class RunPropertiesParser {
	private StartElement startEvent;
	private XMLEventReader events;
	private ConditionalParameters params;
	private StartElement runPropsStartElement;
	private EndElement runPropsEndElement;
	private List<RunProperty> runProperties = new ArrayList<>();
	private XMLEventFactory eventFactory;
	private ElementSkipper generalElementSkipper;

	RunPropertiesParser(StartElement startEvent, XMLEventReader events,
							   ConditionalParameters params, XMLEventFactory eventFactory) {
		this.startEvent = startEvent;
		this.events = events;
		this.params = params;
		this.eventFactory = eventFactory;

		generalElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(params,
				RUN_PROPERTIES_CHANGE,
				RUN_PROPERTY_LANGUAGE,
				RUN_PROPERTY_NO_SPELLING_OR_GRAMMAR);
	}

	RunProperties parse() throws XMLStreamException {
		startRunProps(stripGeneralAttributes(eventFactory, stripRunRevisionAttributes(eventFactory, startEvent)));

		while (events.hasNext()) {
			XMLEvent e = events.nextEvent();
			if (isEndElement(e, startEvent)) {
				endRunProps(e.asEndElement());
				return buildRunProperties();
			}
			if (e.isStartElement()) {
				if (generalElementSkipper.isSkippableElement(e.asStartElement())) {
					generalElementSkipper.skipElement(events, e.asStartElement());
				} else {
					// This gathers the whole event.
					addRunProp(e.asStartElement(), events, isHiddenPropertyElement(e.asStartElement()));
				}
			}
			// Discard -- make sure we're not discarding meaningful data
			else if (e.isCharacters() && !isWhitespace(e)) {
				throw new IllegalStateException(
						"Discarding non-whitespace rPr characters " + e.asCharacters().getData());
			}
		}
		throw new IllegalStateException("Invalid content? Unterminated run properties");
	}

	private void startRunProps(StartElement e) {
		runPropsStartElement = e;
		// DrawingML properties contain inline property attributes that we must parse out
		if (e.getName().getNamespaceURI().equals(Namespaces.DrawingML.getURI())) {
			@SuppressWarnings("rawtypes") Iterator attrs = e.getAttributes();
			while (attrs.hasNext()) {
				// XXX Don't support hidden styles in DrawingML yet
				runProperties.add(RunPropertyFactory.createRunProperty((Attribute)attrs.next()));
			}
		}
	}

	private void addRunProp(StartElement startEl, XMLEventReader events,
						   boolean isHidden) throws XMLStreamException {
		// Gather elements up to the end
		runProperties.add(RunPropertyFactory.createRunProperty(eventFactory, startEl, events, isHidden));
	}

	private void endRunProps(EndElement e) {
		runPropsEndElement = e;
	}

	private RunProperties buildRunProperties() {
		if (runPropsStartElement == null) {
			return RunProperties.emptyRunProperties();
		}
		if (runPropsEndElement == null) {
			throw new IllegalStateException("Incomplete run property markup: " +
						runPropsStartElement.getName());
		}
		return new RunProperties.DefaultRunProperties(
				runPropsStartElement, runPropsEndElement, runProperties);
	}

	private boolean isHiddenPropertyElement(StartElement el) {
		if (params.getTranslateWordHidden()) {
			return false;
		}
		if (el.getName().equals(WPML_PROPERTY_VANISH)) {
			/**
			 * A value of 1 or true is the default value for a boolean attribute of vanish property,
			 * and is implied when the parent element is present, but this attribute is omitted.
			 */
			return getBooleanAttributeValue(el, WPML_VAL, DEFAULT_BOOLEAN_ATTRIBUTE_TRUE_VALUE);
		}
		if (el.getName().equals(XMLEventHelpers.RUN_STYLE)) {
			String style = getAttributeValue(el, WPML_VAL);
			if (style != null && params.tsExcludeWordStyles.contains(style)) {
				return true;
			}
		}
		// TODO: support styles
		return false;
	}
}
