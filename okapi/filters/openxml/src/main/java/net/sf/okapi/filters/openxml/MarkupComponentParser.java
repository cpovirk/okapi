package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

import static net.sf.okapi.filters.openxml.BlockPropertyFactory.createBlockProperty;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createBlockProperties;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;

/**
 * Provides a markup component parser.
 */
class MarkupComponentParser {

    static MarkupComponent parseEmptyElementMarkupComponent(XMLEventReader eventReader, XMLEventFactory eventFactory, StartElement startElement) throws XMLStreamException {
        if (!eventReader.hasNext()) {
            throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
        }

        XMLEvent nextEvent = eventReader.nextEvent();

        if (!isEndElement(nextEvent, startElement)) {
            throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
        }


        return MarkupComponentFactory.createEmptyElementMarkupComponent(eventFactory, startElement, nextEvent.asEndElement());
    }

    static MarkupComponent parseBlockProperties(XMLEventReader eventReader, XMLEventFactory eventFactory, StartElement startElement,
                                                ElementSkipper elementSkipper) throws XMLStreamException {
        List<BlockProperty> properties = new ArrayList<>();

        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();

            if (isEndElement(event, startElement)) {
                return createBlockProperties(eventFactory, startElement, event.asEndElement(), properties);
            }

            if (!event.isStartElement()) {
                continue;
            }

            if (elementSkipper.isSkippableElement(event.asStartElement())) {
                // skip the first level skippable properties
                elementSkipper.skipElement(eventReader, event.asStartElement());
                continue;
            }

            properties.add(createBlockProperty(gatherEventsWithRevisionPropertiesSkipper(eventReader, event.asStartElement(), elementSkipper)));
        }

        throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
    }

    private static List<XMLEvent> gatherEventsWithRevisionPropertiesSkipper(XMLEventReader eventReader, StartElement startElement,
                                                                            ElementSkipper elementSkipper) throws XMLStreamException {
        List<XMLEvent> events = new ArrayList<>();

        events.add(startElement);

        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();

            if (event.isStartElement() && elementSkipper.isSkippableElement(event.asStartElement())) {
                // skip the second level skippable properties
                elementSkipper.skipElement(eventReader, event.asStartElement());
                continue;
            }

            events.add(event);

            if (isEndElement(event, startElement)) {
                return events;
            }
        }

        throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
    }
}
