package net.sf.okapi.filters.openxml;

import java.util.ListIterator;

import static net.sf.okapi.filters.openxml.BlockPropertiesClarifierStrategyFactory.createParagraphPropertiesClarifierStrategy;
import static net.sf.okapi.filters.openxml.BlockPropertiesClarifierStrategyFactory.createTablePropertiesClarifierStrategy;
import static net.sf.okapi.filters.openxml.BlockPropertiesClarifierStrategyFactory.createTextBodyPropertiesClarifierStrategy;

/**
 * Provides a block properties clarifier.
 */
class BlockPropertiesClarifier {

    private BlockPropertiesClarifierStrategy strategy;

    BlockPropertiesClarifier(BlockPropertiesClarifierStrategy strategy) {
        this.strategy = strategy;
    }

    void clarify(ListIterator<MarkupComponent> markupComponentIterator) {
        strategy.clarifyBlockProperties(markupComponentIterator);
    }

    /**
     * Provides a paragraph properties clarifier.
     */
    static class ParagraphPropertiesClarifier extends BlockPropertiesClarifier {

        ParagraphPropertiesClarifier(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
            super(createParagraphPropertiesClarifierStrategy(creationalParameters, clarificationParameters));
        }
    }

    /**
     * Provides a text body properties clarifier.
     */
    static class TextBodyPropertiesClarifier extends BlockPropertiesClarifier {

        TextBodyPropertiesClarifier(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
            super(createTextBodyPropertiesClarifierStrategy(creationalParameters, clarificationParameters));
        }
    }

    /**
     * Provides a table properties clarifier.
     */
    static class TablePropertiesClarifier extends BlockPropertiesClarifier {

        TablePropertiesClarifier(CreationalParameters creationalParameters, ClarificationParameters clarificationParameters) {
            super(createTablePropertiesClarifierStrategy(creationalParameters, clarificationParameters));
        }
    }
}
