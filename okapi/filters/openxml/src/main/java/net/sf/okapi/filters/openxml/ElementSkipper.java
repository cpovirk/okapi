package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_PROPERTY_LANGUAGE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_NAME;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isEndElement;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isStartElement;

/**
 * Provides an element skipper.
 */
class ElementSkipper {

    private ElementSkipperStrategy elementSkipperStrategy;

    protected ElementSkipper(ElementSkipperStrategy elementSkipperStrategy) {
        this.elementSkipperStrategy = elementSkipperStrategy;
    }

    boolean isSkippableElement(StartElement startElement) {
        return elementSkipperStrategy.isSkippableElement(startElement);
    }

    void skipElement(XMLEventReader eventReader, StartElement startElement) throws XMLStreamException {
        elementSkipperStrategy.skipElement(eventReader, startElement);
    }

    /**
     * Provides a general element skipper.
     */
    static class GeneralElementSkipper extends ElementSkipper {

        GeneralElementSkipper(ElementSkipperStrategy elementSkipperStrategy) {
            super(elementSkipperStrategy);
        }

        static boolean isProofingErrorStartElement(XMLEvent event) {
            return isStartElement(event, GeneralInlineSkippableElement.PROOFING_ERROR_ANCHOR.getValue());
        }

        static boolean isInsertedRunContentElement(XMLEvent event) {
            return isStartElement(event, RevisionInlineSkippableElement.RUN_INSERTED_CONTENT.getValue())
                    || isEndElement(event, RevisionInlineSkippableElement.RUN_INSERTED_CONTENT.getValue());
        }

        static boolean isDeletedRunContentStartElement(XMLEvent event) {
            return isStartElement(event, RevisionInlineSkippableElement.RUN_DELETED_CONTENT.getValue());
        }

        static boolean isSectionPropertiesChangeStartElement(XMLEvent event) {
            return isStartElement(event, RevisionPropertySkippableElement.SECTION_PROPERTIES_CHANGE.getValue());
        }

        static boolean isTableGridChangeStartElement(XMLEvent event) {
            return isStartElement(event, RevisionPropertySkippableElement.TABLE_GRID_CHANGE.getValue());
        }

        static boolean isTablePropertiesExceptionsChangeStartElement(XMLEvent event) {
            return isStartElement(event, RevisionPropertySkippableElement.TABLE_PROPERTIES_EXCEPTIONS_CHANGE.getValue());
        }

        static boolean isTableCellPropertiesChangeStartElement(XMLEvent event) {
            return isStartElement(event, RevisionPropertySkippableElement.TABLE_CELL_PROPERTIES_CHANGE.getValue());
        }

        static boolean isTableRowPropertiesChangeStartElement(XMLEvent event) {
            return isStartElement(event, RevisionPropertySkippableElement.TABLE_ROW_PROPERTIES_CHANGE.getValue());
        }

        static void skipElementEvents(XMLEventReader eventReader, StartElement startElement) throws XMLStreamException {
            ElementSkipperStrategy.GeneralElementSkipperStrategy.skipElementEvents(eventReader, startElement);
        }
    }

    /**
     * Provides a bookmark element skipper.
     */
    static class BookmarkElementSkipper extends ElementSkipper {

        BookmarkElementSkipper(ElementSkipperStrategy elementSkipperStrategy) {
            super(elementSkipperStrategy);
        }

        static boolean isBookmarkStartElement(XMLEvent event) {
            return isStartElement(event, GeneralCrossStructureSkippableElement.BOOKMARK_START.getValue());
        }

        static boolean isBookmarkEndElement(XMLEvent event) {
            return isStartElement(event, GeneralCrossStructureSkippableElement.BOOKMARK_END.getValue());
        }
    }

    /**
     * Provides a skippable element interface.
     */
    interface SkippableElement {
        String getValue();
    }

    /**
     * Provides an inline skippable element interface.
     */
    interface InlineSkippableElement extends SkippableElement {}

    /**
     * Provides a general inline skippable element enumeration.
     */
    enum GeneralInlineSkippableElement implements InlineSkippableElement {

        PROOFING_ERROR_ANCHOR("proofErr");

        private String value;

        GeneralInlineSkippableElement(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    /**
     * Provides a revision inline skippable element enumeration.
     */
    enum RevisionInlineSkippableElement implements InlineSkippableElement {

        RUN_INSERTED_CONTENT("ins"),
        RUN_DELETED_CONTENT("del");

        private String value;

        RevisionInlineSkippableElement(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    /**
     * Provides a cross-structure skippable element interface.
     */
    interface CrossStructureSkippableElement extends SkippableElement {}

    /**
     * Provides a general cross-structure skippable element enumeration.
     */
    enum GeneralCrossStructureSkippableElement implements CrossStructureSkippableElement {

        BOOKMARK_START("bookmarkStart"),
        BOOKMARK_END("bookmarkEnd");

        private String value;

        GeneralCrossStructureSkippableElement(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    /**
     * Provides a property skippable element interface.
     */
    interface PropertySkippableElement extends SkippableElement {}

    /**
     * Provides a run property skippable element enumeration.
     */
    enum RunPropertySkippableElement implements PropertySkippableElement {

        RUN_PROPERTY_COMPLEX_SCRIPT_BOLD("bCs"),
        RUN_PROPERTY_LANGUAGE(LOCAL_PROPERTY_LANGUAGE),
        RUN_PROPERTY_NO_SPELLING_OR_GRAMMAR("noProof"),
        RUN_PROPERTY_CHARACTER_SPACING("spacing"),
        RUN_PROPERTY_COMPLEX_SCRIPT_FONT_SIZE("szCs"),
        RUN_PROPERTY_CHARACTER_WIDTH("w"),
        RUN_PROPERTY_VERTICAL_ALIGNMENT("vertAlign");

        private String value;

        RunPropertySkippableElement(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    /**
     * Provides a revision property skippable element enumeration.
     */
    enum RevisionPropertySkippableElement implements PropertySkippableElement {

        RUN_PROPERTY_INSERTED_PARAGRAPH_MARK("ins"),
        RUN_PROPERTY_DELETED_PARAGRAPH_MARK("del"),

        PARAGRAPH_PROPERTIES_CHANGE("pPrChange"),
        RUN_PROPERTIES_CHANGE("rPrChange"),
        SECTION_PROPERTIES_CHANGE("sectPrChange"),
        TABLE_GRID_CHANGE("tblGridChange"),
        TABLE_PROPERTIES_CHANGE("tblPrChange"),
        TABLE_PROPERTIES_EXCEPTIONS_CHANGE("tblPrExChange"),
        TABLE_CELL_PROPERTIES_CHANGE("tcPrChange"),
        TABLE_ROW_PROPERTIES_CHANGE("trPrChange");

        private String value;

        RevisionPropertySkippableElement(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }
}
