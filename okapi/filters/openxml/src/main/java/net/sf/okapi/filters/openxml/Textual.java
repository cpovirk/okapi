package net.sf.okapi.filters.openxml;

/**
 * Interface for translatable chunks of text content, which may
 * or may not be styled.
 */
public interface Textual {

}
