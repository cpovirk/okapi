package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.List;

/**
 * Provides a markup component factory.
 */
class MarkupComponentFactory {

    /**
     * Creates a start markup component.
     *
     * @param eventFactory An event factory
     * @param startElement A start element
     *
     * @return A start markup component
     */
    static MarkupComponent createStartMarkupComponent(XMLEventFactory eventFactory, StartElement startElement) {
        return new MarkupComponent.StartMarkupComponent(eventFactory, startElement);
    }

    /**
     * Creates an end markup component.
     *
     * @param endElement An end element
     *
     * @return An end markup component
     */
    static MarkupComponent createEndMarkupComponent(EndElement endElement) {
        return new MarkupComponent.EndMarkupComponent(endElement);
    }

    /**
     * Creates an empty element markup component.
     *
     * @param eventFactory An event factory
     * @param startElement A start element
     * @param endElement   An end element
     *
     * @return A empty element markup component
     */
    static MarkupComponent createEmptyElementMarkupComponent(XMLEventFactory eventFactory, StartElement startElement, EndElement endElement) {
        return new MarkupComponent.EmptyElementMarkupComponent(eventFactory, startElement, endElement);
    }

    /**
     * Creates a general markup component.
     *
     * @param events Events
     *
     * @return A general markup component
     */
    static MarkupComponent createGeneralMarkupComponent(List<XMLEvent> events) {
        return new MarkupComponent.GeneralMarkupComponent(events);
    }

    /**
     * Creates block properties.
     *
     * @param eventFactory An event factory
     * @param startElement A start element
     * @param endElement   An end element
     * @param properties   Properties
     *
     * @return Block properties
     */
    static MarkupComponent createBlockProperties(XMLEventFactory eventFactory,
                                                 StartElement startElement, EndElement endElement,
                                                 List<BlockProperty> properties) {
        return new BlockProperties(eventFactory, startElement, endElement, properties);
    }
}
