package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

/**
 * Class to parse an individual worksheet and update the shared string
 * data based on worksheet cells and exclusion information.
 */
public class ExcelWorksheet {
	private XMLEventFactory eventFactory;
	private SharedStringMap stringTable;
	private Set<String> excludedColumns;
	private Set<String> excludedColors;
	private ExcelStyles styles;
	private boolean excludeHiddenRowsAndColumns;
	private boolean isSheetHidden;

	public ExcelWorksheet(XMLEventFactory eventFactory, SharedStringMap stringTable,
						  ExcelStyles styles, boolean isSheetHidden, Set<String> excludedColumns,
						  Set<String> excludedColors, boolean excludeHiddenRowsAndColumns) {
		this.eventFactory = eventFactory;
		this.stringTable = stringTable;
		this.styles = styles;
		this.isSheetHidden = isSheetHidden;
		this.excludedColumns = new HashSet<>(excludedColumns); // We may need to modify this locally
		this.excludedColors = excludedColors;
		this.excludeHiddenRowsAndColumns = excludeHiddenRowsAndColumns;
	}

	static final QName ROW = Namespaces.SpreadsheetML.getQName("row");
	static final QName COL = Namespaces.SpreadsheetML.getQName("col");
	static final QName CELL = Namespaces.SpreadsheetML.getQName("c");
	static final QName VALUE = Namespaces.SpreadsheetML.getQName("v");
	static final QName CELL_LOCATION = new QName("r");
	static final QName CELL_TYPE = new QName("t");
	static final QName CELL_STYLE = new QName("s");
	static final QName HIDDEN = new QName("hidden");
	static final QName MIN = new QName("min");
	static final QName MAX = new QName("max");
	void parse(XMLEventReader reader, XMLEventWriter writer) throws IOException, XMLStreamException {
		boolean excluded = false;
		boolean inValue = false;
		boolean isSharedString = false;
		boolean inHiddenRow = false;
		while (reader.hasNext()) {
			XMLEvent e = reader.nextEvent();
			if (e.isStartElement()) {
				StartElement el = e.asStartElement();
				if (el.getName().equals(CELL)) {
					// We only care about cells with @t="s", indicating a shared string
					Attribute typeAttr = el.getAttributeByName(CELL_TYPE);
					if (typeAttr != null && typeAttr.getValue().equals("s")) {
						String currentColumn = getLocationColumn(el.getAttributeByName(CELL_LOCATION).getValue());
						excluded = excludedColumns.contains(currentColumn) || inHiddenRow;
						isSharedString = true;
					}
					Attribute styleAttr = el.getAttributeByName(CELL_STYLE);
					if (styleAttr != null) {
						int styleIndex = Integer.valueOf(styleAttr.getValue());
						ExcelStyles.CellStyle style = styles.getCellStyle(styleIndex);
						// I'm going to start with a naive implementation that should
						// basically be fine, but not ideal if we're excluding large numbers
						// of colors.
						for (String excludedColor : excludedColors) {
							if (style.fill.matchesColor(excludedColor)) {
								excluded = true;
								break;
							}
						}
					}
				}
				else if (el.getName().equals(VALUE)) {
					inValue = true;
				}
				else if (el.getName().equals(ROW)) {
					inHiddenRow = isHidden(el);
				}
				else if (el.getName().equals(COL)) {
					if (isHidden(el)) {
						// Column info blocks span one or more columns, which are referred to
						// via 1-indexed min/max values.
						excludedColumns.addAll(extractColumnNames(el));
					}
				}
			}
			else if (e.isEndElement()) {
				EndElement el = e.asEndElement();
				if (el.getName().equals(CELL)) {
					excluded = false;
					isSharedString = false;
				}
				else if (el.getName().equals(VALUE)) {
					inValue = false;
				}
			}
			else if (e.isCharacters() && inValue && isSharedString) {
				int origIndex = getSharedStringIndex(e.asCharacters().getData());
				int newIndex = stringTable.createEntryForString(origIndex, excluded).getNewIndex();
				// Replace the event with one that contains the new index
				e = eventFactory.createCharacters(String.valueOf(newIndex));
			}
			writer.add(e);
		}
	}

	private boolean isHidden(StartElement el) {
		return excludeHiddenRowsAndColumns &&
				(isSheetHidden || parseOptionalBooleanAttribute(el, HIDDEN, false));
	}

	/**
	 * Check for an attribute that conforms to the XML Schema boolean datatype.  If it is present
	 * (and the value conforms), return the value.  If it is not present, or the value is
	 * non-conforming, return the specified default value.
	 * @param el
	 * @param attrName
	 * @param defaultValue
	 * @return
	 */
	private boolean parseOptionalBooleanAttribute(StartElement el, QName attrName, boolean defaultValue) {
		Attribute a = el.getAttributeByName(attrName);
		if (a == null) return defaultValue;
		String v = a.getValue();
		if ("true".equals(v) || "1".equals(v)) return true;
		if ("false".equals(v) || "0".equals(v)) return false;
		return defaultValue;
	}

	/**
	 * Convert the min and max attributes of a &lt;col&gt; element into a list
	 * of column names.  For example, "min=2; max=2" => [ "B" ].
	 * @param el
	 * @return
	 */
	List<String> extractColumnNames(StartElement el) {
		try {
			List<String> names = new ArrayList<>();
			int min = Integer.valueOf(el.getAttributeByName(MIN).getValue());
			int max = Integer.valueOf(el.getAttributeByName(MAX).getValue());
			for (int i = min; i <= max; i++) {
				names.add(indexToColumnName(i));
			}
			return names;
		}
		catch (NumberFormatException | NullPointerException e) {
			throw new OkapiBadFilterInputException("Invalid <col> element", e);
		}
	}

	static String indexToColumnName(int index) {
		StringBuilder sb = new StringBuilder();

	    while (index > 0) {
	        int modulo = (index - 1) % 26;
	        sb.insert(0, (char)(65 + modulo));
	        index = (index - modulo) / 26;
	    }

	    return sb.toString();
	}

	private static int getSharedStringIndex(String value) {
		try {
			return Integer.valueOf(value);
		}
		catch (NumberFormatException e) {
			throw new IllegalStateException("Unexpected shared string index '" + value + "'");
		}
	}
	private static String getLocationColumn(String location) {
		char buf[] = location.toCharArray();
		for (int i = 0; i < buf.length; i++) {
			if (Character.isDigit(buf[i])) {
				return location.substring(0, i);
			}
		}
		// I don't think this should never happen, so fail fast
		throw new IllegalStateException("Unexpected worksheet cell location '" + location + "'");
	}
}
