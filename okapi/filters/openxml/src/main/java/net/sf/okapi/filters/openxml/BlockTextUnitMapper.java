package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.ISkeleton;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextUnit;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Converts a parsed Block structure into a TextUnit.
 */
public class BlockTextUnitMapper {
	/**
	 * A default code stack pops limit.
	 */
	private static final int DEFAULT_CODE_STACK_POPS_LIMIT = 0;

	/**
	 * An unused run position value.
	 */
	private static final int UNUSED_RUN_POSITION_VALUE = -1;

	private static final String NESTED_ID_GENERATOR_PREFIX = "sub";

	private Block block;
	private IdGenerator id;
	private int nextCodeId = 1;
	private Deque<RunCode> runCodeStack = new ArrayDeque<>();
	private List<ITextUnit> textUnits;
	private Map<Integer, XMLEvents> codeMap = new HashMap<>();
	private List<ITextUnit> referentTus = new ArrayList<>();

	BlockTextUnitMapper(Block block, IdGenerator id) {
		this.block = block;
		this.id = id;
	}

	public List<ITextUnit> getTextUnits() {
		if (textUnits == null) {
			textUnits = process();
		}
		return textUnits;
	}

	class RunCode {
		public RunProperties runProperties;
		public int codeId;
		RunCode(RunProperties runProperties) {
			this.runProperties = runProperties;
			this.codeId = nextCodeId++;
		}
		@Override
		public String toString() {
			return getClass().getSimpleName() + "(" + codeId + ", " + runProperties + ")";
		}
	}

	public List<ITextUnit> process() {
		// Since blocks typically start and end with markup, blocks with <= 2 chunks should
		// be empty.
		if (block.getChunks().size() <= 2) {
			// Sanity check
			for (XMLEvents chunk : block.getChunks()) {
				if (chunk instanceof Run) {
					throw new IllegalStateException(ExceptionMessages.UNEXPECTED_STRUCTURE);
				}
			}
			return Collections.emptyList();
		}
		ITextUnit textUnit = new TextUnit(id.createId());
		textUnit.setPreserveWhitespaces(true);
		TextFragment tf = new TextFragment();
		textUnit.setSource(new TextContainer(tf));

		// The first and last chunks should always be markup.  We skip them.
		List<Chunk> chunks = block.getChunks().subList(1, block.getChunks().size() - 1);
		boolean runHasText = false;
		for (Chunk chunk : chunks) {
			if (chunk instanceof Run) {
				runHasText |= processRun(tf, (Run)chunk, textUnit);
			}
			else if (chunk instanceof RunContainer) {
				RunContainer rc = (RunContainer)chunk;

				if (rc.getRuns().isEmpty()) {
					addIsolatedCode(tf, chunk);
					continue;
				}

				Code openCode = addOpenCode(tf, rc);
				int savedFormattingCodeDepth = runCodeStack.size();
				for (int nestedRunPosition = 0; nestedRunPosition < rc.getRuns().size(); nestedRunPosition++) {
					runHasText |= processNestedRun(tf, rc.getRuns(), textUnit, nestedRunPosition, savedFormattingCodeDepth);
				}
				// Close out any formatting tags that were opened inside the container
				popRunCodesToDepth(tf, savedFormattingCodeDepth);
				addClosedCode(tf, rc, openCode);
			}
			else {
				addIsolatedCode(tf, chunk);
			}
		}
		popAllRunCodes(tf);
		List<ITextUnit> tus = new ArrayList<>();
		tus.addAll(referentTus);
		// Runs containing no text can be skipped, but only if they don't
		// contain a reference to an embedded TU.  (If they do, we need
		// to anchor the skeleton here.  It would be possible to fix this,
		// but would require this class to distinguish deferred TUs from real
		// TUs in its return value, so the part handler could make a decision.)
		if (runHasText || !referentTus.isEmpty()) {
			// Deferred TUs already have their own block skeletons set
			ISkeleton skel = new BlockSkeleton(block, codeMap);
			skel.setParent(textUnit);
			textUnit.setSkeleton(skel);
			tus.add(textUnit);
		}
		return tus;
	}

	private boolean processNestedRun(TextFragment tf, List<Run> runs, ITextUnit textUnit, int runPosition, int codeStackPopsLimit) {
		referentTus.addAll(processNestedBlocks(runs.get(runPosition), textUnit.getId()));
		return addRun(tf,
				codeStackPopsLimit,
				runPosition,
				runs.get(runPosition),
				runPosition + 1 < runs.size() ? runs.get(runPosition + 1) : null);
	}

	private boolean processRun(TextFragment tf, Run run, ITextUnit textUnit) {
		referentTus.addAll(processNestedBlocks(run, textUnit.getId()));
		return addRun(tf, DEFAULT_CODE_STACK_POPS_LIMIT, UNUSED_RUN_POSITION_VALUE, run, null);
	}

	private List<ITextUnit> processNestedBlocks(Run run, String parentId) {
		IdGenerator nestedIds = new IdGenerator(parentId, NESTED_ID_GENERATOR_PREFIX);
		List<ITextUnit> tus = new ArrayList<>();
		for (Textual textual : run.getNestedTextualItems()) {
			if (textual instanceof Block) {
				BlockTextUnitMapper nestedMapper =
						new BlockTextUnitMapper((Block)textual, nestedIds);
				tus.addAll(nestedMapper.process());
			}
			else if (textual instanceof UnstyledText) {
				TextUnit tu = new TextUnit(nestedIds.createId(), ((UnstyledText)textual).getText());
				tu.setPreserveWhitespaces(true);
				tus.add(tu);
			}
		}
		for (ITextUnit tu : tus) {
			tu.setIsReferent(true);
		}
		return tus;
	}

	/**
	 * Adds a run.
	 *
	 * - We produce a single code for the entire structure if a run contains no text or is hidden by styling.
	 * - If the new run is a superset of the old run, then this is a nested tag (this is equivalent to asking if the old
	 *   run is a subset of the new one)
	 *   eg, <b> --> <b><u>
	 * - If the new run is a subset of the old run... end the old tag, start a new tag.
	 *   (Alternately we could go back and redo the whole thing, but that's hard.)
	 * - If the new run has the same properties as the old run, no new tag as needed..
	 *
	 * @param tf                 A text fragment
	 * @param codeStackPopsLimit A code stack pops limit
	 * @param runPosition        A run position
	 * @param run                A run
	 * @param nextRun            A next run
	 *
	 * @return {@code true} - if the run content has been added
	 *         {@code false} - otherwise
	 */
	private boolean addRun(TextFragment tf, int codeStackPopsLimit, int runPosition, Run run, Run nextRun) {
		if (!run.containsText()) {
			// if a run contains no text or is hidden
			addIsolatedCode(tf, run);
			return false;
		}

		RunProperties rp = run.getProperties();

		while (runCodeStack.size() > codeStackPopsLimit
				&& !runCodeStack.isEmpty()
				&& !runCodeStack.peekFirst().runProperties.isSubsetOf(rp)) {
			// if the size of the code stack is more than code stack pops limit
			// or the code stack is not empty
			// or the top of the code stack is not equal to the specified properties

			addCloseCode(tf, runCodeStack.pop());
		}

		if ((0 == runPosition && null != nextRun && !nextRun.getProperties().equals(rp) && !nextRun.getProperties().isSubsetOf(rp))
				|| ((0 < runPosition || 0 < rp.count()) && (runCodeStack.isEmpty() || !runCodeStack.peekFirst().runProperties.equals(rp)))) {
			// if this is the first run and the next run is specified (this should happen only in the run container case processing)
			// or if this is not the first run and the code stack is empty or the top of the code stack is not equal to the specified run properties
			// or if the number of run properties is more than 0 and the code stack is empty or the top of the code stack is not equal to the specified run properties

			RunCode rc = new RunCode(rp);
			runCodeStack.push(rc);
			addOpenCode(tf, rc);
		}

		addRunContent(tf, run);

		return true;
	}

	private void addRunContent(TextFragment tf, Run run) {
		for (XMLEvents runBodyChunk : run.getBodyChunks()) {
			if (runBodyChunk instanceof Run.RunText) {
				addText(tf, ((Run.RunText) runBodyChunk).getText());
			} else {
				// Markup within the run, eg <w:tab/>
				addIsolatedCode(tf, runBodyChunk);
			}
		}
	}

	private void popRunCodesToDepth(TextFragment tf, int desiredDepth) {
		while (runCodeStack.size() > desiredDepth) {
			addCloseCode(tf, runCodeStack.pop());
		}
	}

	private void popAllRunCodes(TextFragment tf) {
		while (!runCodeStack.isEmpty()) {
			addCloseCode(tf, runCodeStack.pop());
		}
	}

	private void addText(TextFragment tf, String text) {
		tf.append(text);
	}

	private Code addOpenCode(TextFragment tf, RunContainer rc) {
		Code code = new Code(TagType.OPENING, RunContainer.Type.HYPERLINK.getValue());
		code.setData("<" + RunContainer.Type.HYPERLINK.getValue() + nextCodeId + ">");
		code.setId(nextCodeId);
		tf.append(code);
		codeMap.put(nextCodeId, rc);
		// Entering the container means we also assume its default properties.
		runCodeStack.push(new RunCode(rc.getDefaultRunProperties()));
		return code;
	}

	private void addClosedCode(TextFragment tf, RunContainer rc, Code openCode) {
		Code code = new Code(TagType.CLOSING, openCode.getType());
		code.setData("</" + RunContainer.Type.HYPERLINK.getValue() + openCode.getId() + ">");
		code.setId(openCode.getId());
		// Clear container default properties
		runCodeStack.pop();
		tf.append(code);
	}

	private void addOpenCode(TextFragment tf, RunCode rc) {
		Code code = new Code(TagType.OPENING, "run");
		code.setId(rc.codeId);
		code.setData("<run" + code.getId() + ">");
		codeMap.put(rc.codeId, rc.runProperties);
		tf.append(code);
	}

	private void addCloseCode(TextFragment tf, RunCode rc) {
		Code code = new Code(TagType.CLOSING, "run");
		code.setId(rc.codeId);
		code.setData("</run" + code.getId() + ">");
		tf.append(code);
	}

	private void addIsolatedCode(TextFragment tf, XMLEvents events) {
		int codeId = nextCodeId++;
		codeMap.put(codeId, events);
		Code code = new Code(TagType.PLACEHOLDER, "x", getCodeData(events, codeId));
		code.setId(codeId);
		tf.append(code);
	}

	private String getCodeData(XMLEvents codeEvents, int codeId) {
		if (codeEvents instanceof Run) {
			return "<run" + codeId + "/>";
		}
		return "<tags" + codeId + "/>";
	}
}
