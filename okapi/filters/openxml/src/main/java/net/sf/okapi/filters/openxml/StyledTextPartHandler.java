package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IdGenerator;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.DocumentPart;
import net.sf.okapi.common.resource.Ending;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.StartSubDocument;
import net.sf.okapi.common.skeleton.ZipSkeleton;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.AttributeStripper.RevisionAttributeStripper.stripSectionPropertiesRevisionAttributes;
import static net.sf.okapi.filters.openxml.AttributeStripper.RevisionAttributeStripper.stripTableRowRevisionAttributes;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.SECTION_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_CELL_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_GRID_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_PROPERTIES_EXCEPTIONS_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.TABLE_ROW_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createEndMarkupComponent;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createGeneralMarkupComponent;
import static net.sf.okapi.filters.openxml.MarkupComponentFactory.createStartMarkupComponent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isBlockMarkupEndEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isBlockMarkupStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isParagraphStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isSectionPropertiesStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTablePropertiesStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTableRowStartEvent;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.isTextBodyPropertiesStartEvent;

/**
 * Part handler for styled text (Word document parts, PPTX slides) that
 * follow the styled run model.
 */
public class StyledTextPartHandler implements OpenXMLPartHandler {
	private OpenXMLZipFile zipFile;
	private ZipEntry entry;

	private ConditionalParameters params;
	private XMLEventFactory eventFactory;
	private String partName;
	private StyleDefinitions styleDefinitions;

	private ElementSkipper tablePropertiesChangeElementSkipper;
	private ElementSkipper noElementSkipper;
	private ElementSkipper revisionPropertyChangeElementSkipper;

	private IdGenerator documentPartId;
	private IdGenerator textUnitId;
	private IdGenerator nestedBlockId;


	private XMLEventReader xmlReader;
	private List<Event> filterEvents = new ArrayList<>();

	private Iterator<Event> filterEventIterator;
	private String docId, subDocId;

	private List<XMLEvent> documentPartEvents = new ArrayList<>();
	private Markup markup = new Block.BlockMarkup();

	StyledTextPartHandler(ConditionalParameters cparams, OpenXMLZipFile zipFile, ZipEntry entry, StyleDefinitions styleDefinitions) {
		this(cparams, zipFile.getEventFactory(), entry.getName(), styleDefinitions);
		this.zipFile = zipFile;
		this.entry = entry;
	}

	StyledTextPartHandler(ConditionalParameters cparams, XMLEventFactory eventFactory, String partName, StyleDefinitions styleDefinitions) {
		this.params = cparams;
		this.eventFactory = eventFactory;
		this.partName = partName;
		this.styleDefinitions = styleDefinitions;

		tablePropertiesChangeElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(TABLE_PROPERTIES_CHANGE);
		noElementSkipper = ElementSkipperFactory.createGeneralElementSkipper();
		revisionPropertyChangeElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
				SECTION_PROPERTIES_CHANGE,
				TABLE_GRID_CHANGE,
				TABLE_PROPERTIES_EXCEPTIONS_CHANGE,
				TABLE_ROW_PROPERTIES_CHANGE,
				TABLE_CELL_PROPERTIES_CHANGE);

		documentPartId = new IdGenerator(partName, IdGenerator.DOCUMENT_PART);
		textUnitId = new IdGenerator(partName, IdGenerator.TEXT_UNIT);
		nestedBlockId = new IdGenerator(null);
	}

	/**
	 * Open this part and perform any initial processing.  Return the
	 * first event for this part.  In this case, it's a START_SUBDOCUMENT
	 * event.
	 *
	 * @param docId document identifier
	 * @param subDocId sub-document identifier
	 * @param srcLang the locale of the source
	 *
	 * @return Event
	 *
	 * @throws IOException
	 * @throws XMLStreamException
     */
	@Override
	public Event open(String docId, String subDocId, LocaleId srcLang) throws IOException, XMLStreamException {
		this.docId = docId;
		this.subDocId = subDocId;
		/**
		 * Process the XML event stream, simplifying as we go.  Non-block content is
		 * written as a document part.  Blocks are parsed, then converted into TextUnit structures.
		 */
		xmlReader = zipFile.getInputFactory().createXMLEventReader(
				new InputStreamReader(new BufferedInputStream(zipFile.getInputStream(entry)), StandardCharsets.UTF_8));
		return open(docId, subDocId, xmlReader);
	}

	// Package-private for test.  XXX This is an artifact of the overall PartHandler
	// interface needing work.
	Event open(String docId, String subDocId, XMLEventReader xmlReader) throws XMLStreamException {
		this.xmlReader = xmlReader;
		try {
			process();
		}
		finally {
			if (xmlReader != null) {
				xmlReader.close();
			}
		}
		return createStartSubDocumentEvent(docId, subDocId);
	}

	private Event createStartSubDocumentEvent(String docId, String subDocId) {
		StartSubDocument sd = new StartSubDocument(docId, subDocId);
		sd.setName(partName);
		if (zipFile != null) { // XXX This null check is a hack for testing
			ZipSkeleton zs = new ZipSkeleton(zipFile.getZip(), entry);
			sd.setSkeleton(zs);
		}
		ConditionalParameters clonedParams = params.clone();
		clonedParams.nFileType = ParseType.MSWORD;
		sd.setFilterParameters(clonedParams);
		return new Event(EventType.START_SUBDOCUMENT, sd);
	}

	private void process() throws XMLStreamException {
		while (xmlReader.hasNext()) {
			XMLEvent e = xmlReader.nextEvent();
			if (isParagraphStartEvent(e)) {
				flushDocumentPart();
				Block block = new BlockParser(e.asStartElement(), xmlReader,
											  eventFactory, params, nestedBlockId, styleDefinitions).parse();
				if (block.isHidden()) {
					documentPartEvents.addAll(block.getEvents());
					continue;
				}
				BlockTextUnitMapper mapper = new BlockTextUnitMapper(block, textUnitId);
				if (mapper.getTextUnits().isEmpty()) {
					addBlockChunksToDocumentPart(block.getChunks());
				}
				else {
					for (ITextUnit tu : mapper.getTextUnits()) {
						filterEvents.add(new Event(EventType.TEXT_UNIT, tu));
					}
				}
			}
			else if (isBlockMarkupStartEvent(e)) {
				addMarkupComponentToDocumentPart(createStartMarkupComponent(eventFactory, e.asStartElement()));
			}
			else if (isTablePropertiesStartEvent(e)) {
				addMarkupComponentToDocumentPart(MarkupComponentParser.parseBlockProperties(xmlReader, eventFactory, e.asStartElement(), tablePropertiesChangeElementSkipper));
			}
			else if (isTextBodyPropertiesStartEvent(e)) {
				addMarkupComponentToDocumentPart(MarkupComponentParser.parseBlockProperties(xmlReader, eventFactory, e.asStartElement(), noElementSkipper));
			}
			else if (isBlockMarkupEndEvent(e)) {
				addMarkupComponentToDocumentPart(createEndMarkupComponent(e.asEndElement()));
			}
			else if (e.isStartElement() && revisionPropertyChangeElementSkipper.isSkippableElement(e.asStartElement())) {
				revisionPropertyChangeElementSkipper.skipElement(xmlReader, e.asStartElement());
			}
			else {
				if (isSectionPropertiesStartEvent(e)) {
					e = stripSectionPropertiesRevisionAttributes(eventFactory, e.asStartElement());
				} else if (isTableRowStartEvent(e)) {
					e = stripTableRowRevisionAttributes(eventFactory, e.asStartElement());
				}
				addEventToDocumentPart(e);
			}
		}
		flushDocumentPart();
		filterEvents.add(new Event(EventType.END_DOCUMENT, new Ending(subDocId)));
		filterEventIterator = filterEvents.iterator();
	}

	private void addEventToDocumentPart(XMLEvent e) {
		documentPartEvents.add(e);
	}

	private void addMarkupComponentToDocumentPart(MarkupComponent markupComponent) {
		if (!documentPartEvents.isEmpty()) {
			markup.addComponent(createGeneralMarkupComponent(documentPartEvents));
			documentPartEvents = new ArrayList<>();
		}
		markup.addComponent(markupComponent);
	}

	private void addBlockChunksToDocumentPart(List<Chunk> chunks) {
		for (Chunk chunk : chunks) {
			if (chunk instanceof Markup) {
				for (MarkupComponent markupComponent : ((Markup) chunk).getComponents()) {
					addMarkupComponentToDocumentPart(markupComponent);
				}
				continue;
			}

			documentPartEvents.addAll(chunk.getEvents());
		}
	}

	private void flushDocumentPart() {
		if (!documentPartEvents.isEmpty()) {
			markup.addComponent(createGeneralMarkupComponent(documentPartEvents));
			documentPartEvents = new ArrayList<>();
		}

		if (!markup.getComponents().isEmpty()) {
			DocumentPart documentPart = new DocumentPart(documentPartId.createId(), false);
			documentPart.setSkeleton(new MarkupSkeleton(markup));
			markup = new Block.BlockMarkup();

			filterEvents.add(new Event(EventType.DOCUMENT_PART, documentPart));
		}
	}

	@Override
	public boolean hasNext() {
		return filterEventIterator.hasNext();
	}

	@Override
	public Event next() {
		return filterEventIterator.next();
	}

	@Override
	public void close() {
	}

	@Override
	public void logEvent(Event e) {
	}
}
