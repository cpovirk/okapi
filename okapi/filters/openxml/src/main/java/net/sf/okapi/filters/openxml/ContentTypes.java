/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/
package net.sf.okapi.filters.openxml;

import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Code to parse the [Content_Types].xml files present in Office OpenXML documents.
 */
class ContentTypes {

	static class Types {
		private static final String PREFIX = "application/vnd.openxmlformats-officedocument.";

		static class Common {
			static final String CORE_PROPERTIES_TYPE =
					"application/vnd.openxmlformats-package.core-properties+xml";
		}

		static class Word {
			static final String MAIN_DOCUMENT_TYPE =
					PREFIX + "wordprocessingml.document.main+xml";
			static final String SETTINGS_TYPE =
					PREFIX + "wordprocessingml.settings+xml";
			static final String STYLES_TYPE =
					PREFIX + "wordprocessingml.styles+xml";
			static final String FOOTER_TYPE =
					PREFIX + "wordprocessingml.footer+xml";
			static final String ENDNOTES_TYPE =
					PREFIX + "wordprocessingml.endnotes+xml";
			static final String HEADER_TYPE =
					PREFIX + "wordprocessingml.header+xml";
			static final String FOOTNOTES_TYPE =
					PREFIX + "wordprocessingml.footnotes+xml";
			static final String COMMENTS_TYPE =
					PREFIX + "wordprocessingml.comments+xml";
			static final String GLOSSARY_TYPE =
					PREFIX + "wordprocessingml.document.glossary+xml";
		}
		static class Drawing {
			static final String CHART_TYPE =
					PREFIX + "drawingml.chart+xml";
			static final String DIAGRAM_TYPE =
					PREFIX + "drawingml.diagramData+xml";
		}
		static class Powerpoint {
			static final String MAIN_DOCUMENT_TYPE =
					PREFIX + "presentationml.presentation.main+xml";
			static final String SLIDE_TYPE =
					PREFIX + "presentationml.slide+xml";
			static final String COMMENTS_TYPE =
					PREFIX + "presentationml.comments+xml";
			static final String NOTES_TYPE =
					PREFIX + "presentationml.notesSlide+xml";
			static final String MASTERS_TYPE =
					PREFIX + "presentationml.slideMaster+xml";
			static final String LAYOUT_TYPE =
					PREFIX + "presentationml.slideLayout+xml";
		}
		static class Excel {
			static final String MAIN_DOCUMENT_TYPE =
					PREFIX + "spreadsheetml.sheet.main+xml";
			static final String SHARED_STRINGS_TYPE =
					PREFIX + "spreadsheetml.sharedStrings+xml";
			static final String WORKSHEET_TYPE =
					PREFIX + "spreadsheetml.worksheet+xml";
			static final String WORKBOOK_TYPE =
					PREFIX + "spreadsheetml.sheet.main+xml";
			static final String COMMENT_TYPE =
					PREFIX + "spreadsheetml.comments+xml";
			static final String TABLE_TYPE =
					PREFIX + "spreadsheetml.table+xml";
			static final String STYLES_TYPE =
					PREFIX + "spreadsheetml.styles+xml";
		}
	}

	static final QName DEFAULT = Namespaces.ContentTypes.getQName("Default");
	static final QName OVERRIDE = Namespaces.ContentTypes.getQName("Override");
	static final QName PARTNAME_ATTR = new QName("PartName");
	static final QName CONTENTTYPE_ATTR = new QName("ContentType");
	static final QName EXTENSION_ATTR = new QName("Extension");
	
	private XMLInputFactory factory;
	private Map<String, String> defaults = new HashMap<String, String>();
	private Map<String, String> overrides = new HashMap<String, String>();
	
	ContentTypes(XMLInputFactory factory) {
		this.factory = factory;
	}
	
	String getContentType(String partName) {
		partName = ensureWellformedPath(partName);
		if (overrides.containsKey(partName)) {
			return overrides.get(partName);
		}
		String suffix = getSuffix(partName);
		if (defaults.containsKey(suffix)) {
			return defaults.get(suffix);
		}
		
		// Unknown file - this shouldn't ever happen.  We 
		// report this as arbitrary data.
		return "application/octet-stream";
	}
	
	void parseFromXML(Reader reader) throws XMLStreamException {
		XMLEventReader eventReader = factory.createXMLEventReader(reader);
		
		while (eventReader.hasNext()) {
			XMLEvent e = eventReader.nextEvent();
			
			if (e.isStartElement()) {
				StartElement el = e.asStartElement();
				if (el.getName().equals(DEFAULT)) {
					Attribute ext = el.getAttributeByName(EXTENSION_ATTR);
					Attribute type = el.getAttributeByName(CONTENTTYPE_ATTR);
					if (ext != null && type != null) {
						defaults.put(ext.getValue(), type.getValue());
					}
				}
				else if (el.getName().equals(OVERRIDE)) {
					Attribute part = el.getAttributeByName(PARTNAME_ATTR);
					Attribute type = el.getAttributeByName(CONTENTTYPE_ATTR);
					if (part != null && type != null) {
						overrides.put(ensureWellformedPath(part.getValue()),
									  type.getValue());
					}
				}
			}
		}
	}
	
	private String getSuffix(String partName) {
		String suffix = partName;
		int i = suffix.lastIndexOf('.');
		if (i != -1) {
			suffix = suffix.substring(i + 1);
		}
		return suffix;
	}
	
	private String ensureWellformedPath(String p) {
		return p.startsWith("/") ? p : "/" + p;
	}
}
