package net.sf.okapi.filters.openxml;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

/**
 * Part Handler for XLSX shared string tables.  This rewrites the shared
 * string table part to order it correct and un-share strings that appear
 * multiple times, then delegates the actual parsing to an OpenXMLContentFilter.
 */
public class SharedStringPartHandler extends ContentFilterBasedPartHandler {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private SharedStringMap sharedStringMap;
	private File rewrittenStringsTable;

	public SharedStringPartHandler(OpenXMLZipFile zipFile, ZipEntry entry, SharedStringMap sharedStringMap,
								   ConditionalParameters cparams) {
		super(new OpenXMLContentFilter(cparams), cparams, zipFile, entry);
		this.sharedStringMap = sharedStringMap;
	}

	interface SharedStringVisibilityStrategy {
		boolean isVisible(int stringIndex);
	}
	class SomeStringsExcluded implements SharedStringVisibilityStrategy {
		@Override
		public boolean isVisible(int stringIndex) {
			return sharedStringMap.isStringVisible(stringIndex);
		}
	}

	@Override
	public Event open(String docId, String subDocId, LocaleId srcLang)
			throws IOException {
		try {
			SharedStringsDenormalizer deno = new SharedStringsDenormalizer(zipFile.getEventFactory(), sharedStringMap);
			XMLEventReader reader = zipFile.getInputFactory().createXMLEventReader(
					new InputStreamReader(zipFile.getInputStream(entry), StandardCharsets.UTF_8));
			rewrittenStringsTable = File.createTempFile("sharedStrings", ".xml");
			XMLEventWriter writer = zipFile.getOutputFactory().createXMLEventWriter(
					new OutputStreamWriter(new FileOutputStream(rewrittenStringsTable), StandardCharsets.UTF_8));
			deno.process(reader, writer);

			// The string table has been rewritten according to the map data.  Now expose
			// the new table for translation.
			contentFilter.setUpConfig(ParseType.MSEXCEL);
			contentFilter.setPartName(entry.getName());

			// Tie string visibility to color and column excludes
			contentFilter.setExcelVisibleSharedStrings(new SomeStringsExcluded());

			InputStream is = new BufferedInputStream(new FileInputStream(rewrittenStringsTable));
			return openContentFilter(is, docId, subDocId, srcLang);
		}
		catch (XMLStreamException e) {
			throw new OkapiBadFilterInputException(e);
		}
	}

	@Override
	public void close() {
		super.close();
		if (!rewrittenStringsTable.delete()) {
			LOGGER.warn("Couldn't clean up temp file.");
		}
	}
}
