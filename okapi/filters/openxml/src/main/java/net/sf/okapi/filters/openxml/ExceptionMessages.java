package net.sf.okapi.filters.openxml;

/**
 * Provides exception messages.
 */
class ExceptionMessages {
    static final String UNEXPECTED_STRUCTURE = "Unexpected structure";
}
