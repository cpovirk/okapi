package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.ContentTypes.Types.Common;
import static net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import static net.sf.okapi.filters.openxml.ContentTypes.Types.Word;
import static net.sf.okapi.filters.openxml.ParseType.MSWORDCHART;
import static net.sf.okapi.filters.openxml.ParseType.MSWORDDOCPROPERTIES;

class WordDocument extends DocumentType {
	private static final String STYLE_DEFINITIONS_SOURCE_TYPE = Namespaces.DocumentRelationships.getDerivedURI("/styles");

	WordDocument(OpenXMLZipFile zipFile, ConditionalParameters params) {
		super(zipFile, params);
	}

	@Override
	void initialize() throws IOException, XMLStreamException {
		setStyleDefinitions(parseStyleDefinitions());
	}

	private StyleDefinitions parseStyleDefinitions() throws IOException, XMLStreamException {
		String relationshipTarget = getRelationshipTarget();

		if (null == relationshipTarget) {
			return StyleDefinitions.emptyStyleDefinitions();
		}

		Reader reader = getZipFile().getPartReader(relationshipTarget);

		return new WordProcessingStylesParser(
				getZipFile().getEventFactory(),
				getZipFile().getInputFactory(),
				reader,
				getParams()
		).parse();
	}

	private String getRelationshipTarget() throws IOException, XMLStreamException {
		String mainDocumentTarget = getZipFile().getMainDocumentTarget();
		Relationships relationships = getZipFile().getRelationshipsForTarget(mainDocumentTarget);
		List<Relationships.Rel> rels = relationships.getRelByType(STYLE_DEFINITIONS_SOURCE_TYPE);

		if (null == rels) {
			return null;
		}

		return rels.get(0).target;
	}

	@Override
	OpenXMLPartHandler getHandlerForFile(ZipEntry entry,
			String contentType) {
		// Check to see if this is non-translatable
		if (!isTranslatableType(entry.getName(), contentType)) {
			return new NonTranslatablePartHandler(getZipFile(), entry);
		}

		if (isStyledTextPart(entry.getName(), contentType)) {
			return new StyledTextPartHandler(getParams(), getZipFile(), entry, getStyleDefinitions());
		}
		OpenXMLContentFilter openXMLContentFilter = new OpenXMLContentFilter(getParams());
		openXMLContentFilter.setPartName(entry.getName());
		ParseType parseType = ParseType.MSWORD;
		if (Word.SETTINGS_TYPE.equals(contentType)) {
			openXMLContentFilter.setBInSettingsFile(true);
		}
		else if (Drawing.CHART_TYPE.equals(contentType)) {
			parseType = MSWORDCHART;
		}
		else if (Common.CORE_PROPERTIES_TYPE.equals(contentType)) {
			parseType = MSWORDDOCPROPERTIES;
		}
		openXMLContentFilter.setUpConfig(parseType);

		// From openSubDocument
		if (!getParams().getTranslateWordHidden() || !getParams().getTranslateWordAllStyles()) {
			openXMLContentFilter.setTsExcludeWordStyles(getParams().tsExcludeWordStyles);
		}
		return new StandardPartHandler(openXMLContentFilter, getParams(), getZipFile(), entry);
	}

	@Override
	boolean isClarifiablePart(String contentType) {
		return false;
	}

	@Override
	boolean isStyledTextPart(String entryName, String type) {
		return (type.equals(Word.MAIN_DOCUMENT_TYPE) ||
				type.equals(Word.HEADER_TYPE) ||
				type.equals(Word.FOOTER_TYPE) ||
				type.equals(Word.ENDNOTES_TYPE) ||
				type.equals(Word.FOOTNOTES_TYPE) ||
				type.equals(Word.GLOSSARY_TYPE)) ||
				type.equals(Word.COMMENTS_TYPE) ||
				type.equals(Drawing.DIAGRAM_TYPE);
	}

	private boolean isTranslatableType(String entryName, String type) {
		if (!entryName.endsWith(".xml")) return false;
		if (type.equals(Word.MAIN_DOCUMENT_TYPE)) return true;
		if (type.equals(Word.STYLES_TYPE)) return true;
		if (getParams().getTranslateDocProperties() && type.equals(Common.CORE_PROPERTIES_TYPE)) return true;
		if (type.equals(Word.HEADER_TYPE) || type.equals(Word.FOOTER_TYPE)) {
			return getParams().getTranslateWordHeadersFooters();
		}
		if (type.equals(Word.COMMENTS_TYPE)) {
			return getParams().getTranslateComments();
		}
		if (type.equals(Word.SETTINGS_TYPE)) return true;
		if (type.equals(Drawing.CHART_TYPE)) return true;
		if (isStyledTextPart(entryName, type)) return true;
		return false;
	}

	@Override
	Enumeration<? extends ZipEntry> getZipFileEntries() {
		List<? extends ZipEntry> list = Collections.list(getZipFile().entries());
		List<String> additionalParts = new ArrayList<String>();
		additionalParts.add("word/styles.xml");
		additionalParts.add("word/_rels/document.xml.rels");
		additionalParts.add("word/document.xml");
		Collections.sort(list, new ZipEntryComparator(additionalParts));
		return Collections.enumeration(list);
	}
}
