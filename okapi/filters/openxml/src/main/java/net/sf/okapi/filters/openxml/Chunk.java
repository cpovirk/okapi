package net.sf.okapi.filters.openxml;

/**
 * Marker interface to distinguish XMLEvents implementation that can be added to a Block.
 */
public interface Chunk extends XMLEvents {
}
