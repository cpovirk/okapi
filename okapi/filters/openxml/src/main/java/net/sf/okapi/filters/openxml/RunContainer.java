package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * A markup structure, such as a hyperlink, that can contain
 * multiple child runs.
 *
 * Runs within this container can be simplified and consolidated, but
 * can't be consolidated with runs outside the container.  When exposed
 * as ITextUnit content, the container boundaries should appear as a single
 * set of paired codes.
 */
public class RunContainer implements Block.BlockChunk {
	private StartElement startElement;
	private EndElement endElement;

	private List<Run> runs = new ArrayList<>();

	RunContainer(StartElement startElement, EndElement endElement, List<Run> runs) {
		this.startElement = startElement;
		this.endElement = endElement;
		this.runs.addAll(runs);
	}

	public List<Run> getRuns() {
		return runs;
	}

	public StartElement getStartElement() {
		return startElement;
	}

	public EndElement getEndElement() {
		return endElement;
	}

	public boolean containsText() {
		for (Run run : getRuns()) {
			if (run.containsText()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * The container assumes the properties of the first run are its "default",
	 * so that no additional code is needed to write them.
	 */
	public RunProperties getDefaultRunProperties() {
		return runs.get(0).getProperties();
	}

	@Override
	public List<XMLEvent> getEvents() {
		List<XMLEvent> events = new ArrayList<>();
		events.add(startElement);
		for (Run run : runs) {
			events.addAll(run.getEvents());
		}
		events.add(endElement);
		return events;
	}

	@Override
	public String toString() {
		return "RunContainer(" + XMLEventSerializer.serialize(startElement) + ", "+ runs.size() +
				")[" + runs  + "]";
	}

	/**
	 * Provides run container types.
	 */
	enum Type {
		HYPERLINK("hyperlink");

		String value;

		Type(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
}
