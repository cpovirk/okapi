package net.sf.okapi.filters.openxml;

/**
 * Provides style types.
 */
enum StyleType {
    INVALID(""),
    NUMBERING("numbering"),
    TABLE("table"),

    PARAGRAPH("paragraph"),
    CHARACTER("character");

    String value;

    StyleType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static StyleType fromValue(String value) {
        if (null == value) {
            return INVALID;
        }

        for (StyleType attributeValue : values()) {
            if (value.equals(attributeValue.getValue())) {
                return attributeValue;
            }
        }

        return INVALID;
    }
}

