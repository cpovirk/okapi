/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.properties;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.filters.LocalizationDirectives;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {

	private static final String USELD = "useLd";
	private static final String LOCALIZEOUTSIDE = "localizeOutside";
	private static final String USECODEFINDER = "useCodeFinder";
	private static final String CODEFINDERRULES = "codeFinderRules";

	private static final String CONVERTLFANDTAB = "convertLFandTab";
	private static final String USEKEYCONDITION = "useKeyCondition";
	private static final String EXTRACTONLYMATCHINGKEY = "extractOnlyMatchingKey";
	private static final String KEYCONDITION = "keyCondition";
	private static final String EXTRACOMMENTS = "extraComments";
	private static final String COMMENTSARENOTES = "commentsAreNotes";
	private static final String ESCAPEEXTENDEDCHARS = "escapeExtendedChars";
	private static final String SUBFILTER = "subfilter";
	
	public InlineCodeFinder codeFinder;
	public LocalizationDirectives locDir;
	
	public Parameters () {
		super();
	}
	
	public void reset () {
		super.reset();
		locDir = new LocalizationDirectives();

		setEscapeExtendedChars(true);
		setConvertLFandTab(true);
		setUseKeyCondition(false);;
		setExtractOnlyMatchingKey(true);
		setKeyCondition(".*text.*");
		setExtraComments(false);
		setCommentsAreNotes(true);
		setSubfilter(null);

		setUseCodeFinder(true);
		codeFinder = new InlineCodeFinder();	
		codeFinder.setSample("%s, %d, {1}, \\n, \\r, \\t, etc.");
		codeFinder.setUseAllRulesWhenTesting(true);
		// Default in-line codes: special escaped-chars and printf-style variable
		codeFinder.addRule("%(([-0+#]?)[-0+#]?)((\\d\\$)?)(([\\d\\*]*)(\\.[\\d\\*]*)?)[dioxXucsfeEgGpn]");
		codeFinder.addRule("(\\\\r\\\\n)|\\\\a|\\\\b|\\\\f|\\\\n|\\\\r|\\\\t|\\\\v");
		//TODO: Add Java-style variables. this is too basic
		codeFinder.addRule("\\{\\d[^\\\\]*?\\}");		
		// Basic HTML/XML
		codeFinder.addRule("\\<(/?)\\w+[^>]*?>");
		setSimplifierRules(null);
	}

	@Override
	public String toString () {
		buffer.setBoolean(USELD, locDir.useLD());
		buffer.setBoolean(LOCALIZEOUTSIDE, locDir.localizeOutside());
		buffer.setGroup(CODEFINDERRULES, codeFinder.toString());

		return super.toString();
	}
	
	public void fromString (String data) {
		super.fromString(data);

		boolean tmpBool1 = buffer.getBoolean(USELD, locDir.useLD());
		boolean tmpBool2 = buffer.getBoolean(LOCALIZEOUTSIDE, locDir.localizeOutside());
		locDir.setOptions(tmpBool1, tmpBool2);
		codeFinder.fromString(buffer.getGroup(CODEFINDERRULES, ""));
	}

	public boolean isUseCodeFinder() {
		return getBoolean(USECODEFINDER);
	}

	public void setUseCodeFinder(boolean useCodeFinder) {
		setBoolean(USECODEFINDER, useCodeFinder);
	}

	public InlineCodeFinder getCodeFinder() {
		return codeFinder;
	}

	public void setCodeFinder(InlineCodeFinder codeFinder) {
		this.codeFinder = codeFinder;
	}

	public boolean isEscapeExtendedChars() {
		return getBoolean(ESCAPEEXTENDEDCHARS);
	}

	public void setEscapeExtendedChars(boolean escapeExtendedChars) {
		setBoolean(ESCAPEEXTENDEDCHARS, escapeExtendedChars);
	}

	public boolean isUseKeyCondition() {
		return getBoolean(USEKEYCONDITION);
	}

	public void setUseKeyCondition(boolean useKeyCondition) {
		setBoolean(USEKEYCONDITION, useKeyCondition);
	}

	public boolean isExtractOnlyMatchingKey() {
		return getBoolean(EXTRACTONLYMATCHINGKEY);
	}

	public void setExtractOnlyMatchingKey(boolean extractOnlyMatchingKey) {
		setBoolean(EXTRACTONLYMATCHINGKEY, extractOnlyMatchingKey);
	}

	public String getKeyCondition() {
		return getString(KEYCONDITION);
	}

	public void setKeyCondition(String keyCondition) {
		setString(KEYCONDITION, keyCondition);
	}

	public boolean isExtraComments() {
		return getBoolean(EXTRACOMMENTS);
	}

	public void setExtraComments(boolean extraComments) {
		setBoolean(EXTRACOMMENTS, extraComments);
	}

	public boolean isCommentsAreNotes() {
		return getBoolean(COMMENTSARENOTES);
	}

	public void setCommentsAreNotes(boolean commentsAreNotes) {
		setBoolean(COMMENTSARENOTES, commentsAreNotes);
	}

	public LocalizationDirectives getLocDir() {
		return locDir;
	}

	public void setLocDir(LocalizationDirectives locDir) {
		this.locDir = locDir;
	}

	public boolean isConvertLFandTab() {
		return getBoolean(CONVERTLFANDTAB);
	}

	public void setConvertLFandTab(boolean convertLFandTab) {
		setBoolean(CONVERTLFANDTAB, convertLFandTab);
	}

	public String getSubfilter() {
		return getString(SUBFILTER);
	}

	public void setSubfilter(String subfilter) {
		setString(SUBFILTER, subfilter);
	}
	
	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}
}
