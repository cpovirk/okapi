/*===========================================================================
  Copyright (C) 2009-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.yaml;

import net.sf.okapi.common.ISimplifierRulesParameters;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.filters.InlineCodeFinder;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.core.simplifierrules.ParseException;
import net.sf.okapi.core.simplifierrules.SimplifierRules;

public class Parameters extends StringParameters implements ISimplifierRulesParameters {

	private static final String EXTRACTISOLATEDSTRINGS = "extractIsolatedStrings";
	private static final String EXTRACTALLPAIRS = "extractAllPairs";
	private static final String EXCEPTIONS = "exceptions";
	private static final String USEKEYASNAME = "useKeyAsName";
	private static final String USECODEFINDER = "useCodeFinder";
	private static final String USEFULLKEYPATH = "useFullKeyPath";
	private static final String CODEFINDERRULES = "codeFinderRules";
	private static final String SUBFILTER = "subfilter"; 
	private static final String ESCAPENONASCII = "escapeNonAscii";
	private static final String WRAP = "wrap";

	private InlineCodeFinder codeFinder; // Initialized in reset()

	public Parameters () {
		super();
	}
	
	public boolean getExtractStandalone () {
		return getBoolean(EXTRACTISOLATEDSTRINGS);
	}

	public void setExtractStandalone (boolean extractStandalone) {
		setBoolean(EXTRACTISOLATEDSTRINGS, extractStandalone);
	}

	public boolean getExtractAllPairs () {
		return getBoolean(EXTRACTALLPAIRS);
	}

	public void setExtractAllPairs (boolean extractAllPairs) {
		setBoolean(EXTRACTALLPAIRS, extractAllPairs);
	}

	public String getExceptions () {
		return getString(EXCEPTIONS);
	}

	public void setExceptions (String exceptions) {
		setString(EXCEPTIONS, exceptions);
	}

	public boolean getUseKeyAsName () {
		return getBoolean(USEKEYASNAME);
	}

	public void setUseKeyAsName (boolean useKeyAsName) {
		setBoolean(USEKEYASNAME, useKeyAsName);
	}
	
	public boolean getUseFullKeyPath () {
		return getBoolean(USEFULLKEYPATH);
	}

	public void setUseFullKeyPath (boolean useFullKeyPath) {
		setBoolean(USEFULLKEYPATH, useFullKeyPath);
	}

	public boolean getUseCodeFinder () {
		return getBoolean(USECODEFINDER);
	}

	public void setUseCodeFinder (boolean useCodeFinder) {
		setBoolean(USECODEFINDER, useCodeFinder);
		if (getUseCodeFinder()) {
			setSubfilter("");
		}
	}

	public InlineCodeFinder getCodeFinder () {
		return codeFinder;
	}
	
	public String getSubfilter() {
		return getString(SUBFILTER);
	}
	
	public void setSubfilter(String subfilter) {
		setString(SUBFILTER, subfilter);
		if (!"".equals(getSubfilter())) {
			setUseCodeFinder(false);
		}
	}

	public String getCodeFinderData () {
		return codeFinder.toString();
	}

	public void setCodeFinderData (String data) {
		codeFinder.fromString(data);
	}
	
	public boolean getEscapeNonAscii() {
		return getBoolean(ESCAPENONASCII);
	}
	
	public void setEscapeNonAscii (boolean escapeNonAscii) {
		setBoolean(ESCAPENONASCII, escapeNonAscii);
	}	
	
	public boolean isWrap() {
		return getBoolean(WRAP);
	}
	
	public void setWrap(boolean wrap) {
		setBoolean(WRAP, wrap);
	}	
	
	@Override
	public String getSimplifierRules() {
		return getString(SIMPLIFIERRULES);
	}

	@Override
	public void setSimplifierRules(String rules) {
		setString(SIMPLIFIERRULES, rules);		
	}

	@Override
	public void validateSimplifierRules() throws ParseException {
		SimplifierRules r = new SimplifierRules(getSimplifierRules(), new Code());
		r.parse();
	}

	public void reset () {
		super.reset();
		setExtractStandalone(false);
		setExtractAllPairs(true);
		setExceptions("");
		setUseKeyAsName(true);
		setUseFullKeyPath(true);
		setUseCodeFinder(true);
		setSubfilter(null);
		codeFinder = new InlineCodeFinder();
		codeFinder.setSample("%s, %d, {1}, \\n, \\r, \\t, {{var}} etc.");
		codeFinder.setUseAllRulesWhenTesting(true);
		codeFinder.addRule("%(([-0+#]?)[-0+#]?)((\\d\\$)?)(([\\d\\*]*)(\\.[\\d\\*]*)?)[dioxXucsfeEgGpnYyBbHhSMmAZ]");
		codeFinder.addRule("(\\\\r\\\\n)|\\\\a|\\\\b|\\\\f|\\\\n|\\\\r|\\\\t|\\\\v");
		codeFinder.addRule("\\{\\{\\w.*?\\}\\}");
		setEscapeNonAscii(false);
		setWrap(true);
		setSimplifierRules(null);
	}

	public void fromString (String data) {
		super.fromString(data);
		codeFinder.fromString(buffer.getGroup(CODEFINDERRULES, ""));
	}
	
	@Override
	public String toString () {
		buffer.setGroup(CODEFINDERRULES, codeFinder.toString());
		return super.toString();
	}

}
