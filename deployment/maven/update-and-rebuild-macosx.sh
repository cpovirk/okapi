#!/bin/bash -e

cd ../..
cd okapi-ui/swt/core-ui/
mvn -PCOCOA_64_SWT dependency:resolve
cd ../../..

mvn clean -q
mvn install -q -TC8 -DskipITs

cd deployment/maven
ant -f build_getVerProp.xml

ant -f build_okapi-lib.xml

ant -f build_okapi-apps.xml -Dplatform=cocoa-macosx-x86_64

ant -f build_okapi-plugins.xml

chmod a+x dist_cocoa-macosx-x86_64/tikal.sh
chmod a+x dist_cocoa-macosx-x86_64//Rainbow.app/Contents/MacOS/rainbow.sh

# rainbow and tikal tests
cd ../../applications/integration-tests
mvn clean -q -TC8 integration-test

# build okapi SDK artifacts
#cd ../deployment
#mvn clean install -q -TC4 -U


